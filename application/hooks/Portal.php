<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portal {
	private $CI;
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
	public function checkLogin() {
		if($this->CI->uri->segment(1) != 'login' && !$this->CI->session->userdata('userID') ) {
			header('Location: ' . site_url() . 'login' );
			exit();
		}
	}
}
// END Portal class

/* End of file Portal.php */
/* Location: ./application/hooks/Portal.php */