<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MX_Controller {
	
	private $_body;
	private $_baseUrl;
	
	function __construct() {
		parent::__construct();
		$this->_baseUrl = base_url();
		$this->_body = array();
	}
	
	public function index() {
		/**
		 * LOGIN
		 */
		//echo $this->my_crypto->password('123456');
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_body['URL_POST_LOGIN'] = $this->_baseUrl . 'login/loginIn';
		$this->_body['MSG_ERROR'] = ($this->session->flashdata('errors')) ? $this->session->flashdata('errors') : 'Ingresar al sistema';
		$this->_body['CLASS_ERROR'] = ($this->session->flashdata('errors')) ? 'alert-error' : '';
		$this->_body['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_body['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->parser->parse('login/login', $this->_body);
	}
	
	public function loginIn() {
		if($_POST)
		{
			$username = $this->security->xss_clean($this->input->post('username', TRUE));
			$password = $this->security->xss_clean($this->input->post('password', TRUE));
			
			$data = array(
				'user'	=> $username
				,'password'	=> $password
			);
			$login = $this->my_login->loginIn($data);
			if($login)
			{
				$this->acl = new $this->my_acl($this->session->userdata('userID'));
				header('Location: ' . site_url());
				//echo json_encode(array('success' => true));
			} else {
				$flash = $this->session->flashdata('errors');
				/*echo json_encode(array(
					'errors'	=> $flash
				));*/
				header('Location: ' . site_url() . 'login');
			}
		}
	}
	
	public function loginOut() {
		if($this->my_login->loginOut())
		{
			Header('Location: '. base_url());
		}
	}
}

/* End of file login.php */
/* Location: ./application/modules/login/controllers/login.php */
