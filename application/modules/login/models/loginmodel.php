<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class loginModel extends MX_Controller {
	
	private $tableL = 'users';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function loginIn($where)
	{
		$query = $this->db->get_where($this->tableL, $where);
		return $query;
		
	}
}
/* End of file loginmodel.php */
/* Location: ./application/modules/login/models/loginmodel.php */
