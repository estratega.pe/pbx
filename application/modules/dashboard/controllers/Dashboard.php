<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_header;
	private $_body;
	private $_footer;
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		//$this->acl->acceso('admin_access');
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_body = array();
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
	}
	
	public function index()
	{
		modules::run('reports/general/index');
	}
}