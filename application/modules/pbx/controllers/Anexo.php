<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anexo extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/anexo/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_anexo_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function anexo
	 * List all anexos
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_anexo_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET ANEXOS
		$anexos = $this->model->getData('ps_endpoints');
		$anexo = array();
		$tAnexo = 0;
		foreach($anexos  as $k) {
			if(preg_match("/SIP_/", $k->id) || preg_match("/TRUNK_/", $k->id)) {
				continue;
			}
			$show = (!$this->my_acl->acceso('pbx_anexo_show')) ? '' : anchor(base_url() . $this->_moduleUrl . 'anexoShow/' . $k->id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('pbx_anexo_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'anexoEdit/' . $k->id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = (!$this->my_acl->acceso('pbx_anexo_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'anexoDel/' . $k->id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($anexo, array(
				'ANEXO'		=> $k->id
				,'CONTEXT'	=> $k->context
				,'CODECS'	=> $k->allow
				,'ACTIONS'	=> $show
				. $edit
				. $delete
			));
			$tAnexo++;
		}
		$totalAnexoCfg = $this->model->getDataRow('cdp_core_config', array('cdp_key' => 'max_anexo'))->cdp_value;
		$totalAnexo = count($anexos);
		if( $totalAnexoCfg > $tAnexo ) {
			$this->_data['LINK_ANEXO_ADD'] = (!$this->my_acl->acceso('pbx_anexo_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'anexoAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
			$this->_data['TITLE_BODY'] = 'ANEXO (' . $tAnexo . '/' . $totalAnexoCfg . ')';
		} else {
			$this->_data['LINK_ANEXO_ADD'] = 'Maximo de anexos creados ';
			$this->_data['TITLE_BODY'] = '(' . $tAnexo . '/' . $totalAnexoCfg . ')';
		}
		$this->_data['ANEXO_ITEM'] = $anexo;
		$body = $this->parser->parse('pbx/anexo/anexo', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'ANEXO';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de ANEXO';
		generatePage($this->_body);
	}
	
	/**
	 * function anexoAdd
	 * Add new Anexo
	 */
	public function anexoAdd() {
		$anexos = $this->model->getData('ps_endpoints');
		$anexo = array();
		$tAnexo = 0;
		foreach($anexos  as $k) {
			if(preg_match("/SIP_/", $k->id) || preg_match("/TRUNK_/", $k->id)) {
				continue;
			}
			$tAnexo++;
		}
		if(!$this->my_acl->acceso('pbx_anexo_add') || ($tAnexo >= $this->model->getDataRow('cdp_core_config', array('cdp_key' => 'max_anexo'))->cdp_value) ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$anexo = $this->security->xss_clean($this->input->post('anexo', TRUE));
			$password = $this->security->xss_clean($this->input->post('password', TRUE));
			$context = 'internos';
			$codecs = 'g729';
			//$context = $this->security->xss_clean($this->input->post('context', TRUE));
			//$codecs = $this->security->xss_clean($this->input->post('codecs', TRUE));
			//$codecs = trim(str_replace(' ','',$codecs));
			//if(empty($anexo) || empty($password) || empty($context) || empty($codecs) ) {
			if(empty($anexo) || empty($password) ) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'anexoAdd');
				exit();
			}
			$ps_auths = array(
				'id'	=> $anexo
				,'auth_type'	=> 'userpass'
				,'password'	=> $password
				,'username'	=> $anexo
			);
			$this->model->createData('ps_auths', $ps_auths);
			$ps_aors = array(
				'id'	=> $anexo
				,'mailboxes'	=> $anexo
				,'max_contacts'	=> 2
				,'remove_existing' => 'yes'
			);
			$this->model->createData('ps_aors', $ps_aors);
			$ps_endpoints = array(
				'id'	=> $anexo
				,'transport'	=> 'transport-udp'
				,'aors'	=> $anexo
				,'auth'	=> $anexo
				,'context'	=> $context
				,'disallow'	=> 'all'
				,'allow'	=> $codecs
				,'direct_media'	=> 'no'
				,'from_user'	=> $anexo
				,'rtp_symmetric'	=> 'yes'
				,'send_rpid'	=> 'yes'
				,'allow_subscribe'	=> 'yes'
				,'rewrite_contact'	=> 'no'
				,'dtmf_mode'	=> 'info'
				,'mailboxes'	=> $anexo.'@'.$context
				,'aggregate_mwi'	=> 'yes'
				,'allow_subscribe' => 'yes'
			);
			$this->model->createData('ps_endpoints', $ps_endpoints);
			pbxRegeneratePJSip();
			header('Location: ' . base_url() . $this->_moduleUrl . 'anexoShow/' . $anexo);
			exit();
		}
		/**
		 * BODY
		 */
		$pass = generatePasswordText();
		$contexts = $this->model->getData('context');
		$context_options = array();
		foreach($contexts as $k) {
			$context_options[$k->context_name] = $k->context_name;
		}
		$this->_data['CONTEXT_SELECT'] = form_dropdown('context', $context_options, 'X', 'class="form-control"');
		$auth_rejection_permanent = array('yes' => 'SI','no' => 'NO');
		$this->_data['SELECT_AUTH_REJECTION_PERMANENT'] = form_dropdown('auth_rejection_permanent', $auth_rejection_permanent, 'no', 'class="form-control"');
		$this->_data['TITLE_BODY'] = "ANEXO";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['PASSWORD'] = $this->my_crypto->password($pass);
		$body = $this->parser->parse('pbx/anexo/anexo-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Anexo|';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Anexo';
		generatePage($this->_body);
	}
	/**
	 * function anexosShow
	 * Show a Anexo
	 */
	public function anexoShow() {
		$anexoId = $this->uri->segment(4, TRUE);
		$anexoId = filter_var($anexoId,FILTER_VALIDATE_INT) ? $anexoId : 0;
		if($anexoId == 0x0000 || !$this->my_acl->acceso('pbx_anexo_show') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET ANEXO BY ID
		$anexo = $this->model->getDataRow('ps_endpoints', array('id' => $anexoId));
		/**
		 * BODY
		 */
		$_a = $this->model->getDataRow('ps_auths', array('id' => $anexo->id));
		$_e = $this->model->getDataRow('ps_aors', array('id' => $anexo->id));
		$this->_data['TITLE_BODY'] = "ANEXO";
		$this->_data['ANEXO'] = $_a->username;
		$this->_data['PASSWORD'] = $_a->password;
		$this->_data['CONTEXT'] = $anexo->context;
		$this->_data['CODECS'] = $anexo->allow;
		$body = $this->parser->parse('pbx/anexo/anexo-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Anexo';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Anexo ' . $anexo->id;
		generatePage($this->_body);
	}
	/**
	 * function anexoEdit
	 * Edit a Anexo
	 */
	public function anexoEdit() {
		$anexoId = $this->uri->segment(4, TRUE);
		$anexoId = filter_var($anexoId,FILTER_VALIDATE_INT) ? $anexoId : 0;
		if($anexoId == 0x0000 || !$this->my_acl->acceso('pbx_anexo_edit') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$anexo = $this->security->xss_clean($this->input->post('anexo', TRUE));
			$password = $this->security->xss_clean($this->input->post('password', TRUE));
			$context = 'internos';
			$codecs = 'g729';
			//$context = $this->security->xss_clean($this->input->post('context', TRUE));
			//$codecs = $this->security->xss_clean($this->input->post('codecs', TRUE));
			$codecs = trim(str_replace(' ','',$codecs));
			if(empty($anexo) || empty($password) || empty($context) || empty($codecs) || $anexo != $anexoId ) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'anexoEdit/' . $anexoId);
				exit();
			}
			$ps_auths = array(
				'auth_type'	=> 'userpass'
				,'password'	=> $password
			);
			$this->model->updateData('ps_auths', array('id' => $anexo), $ps_auths);
			$ps_aors = array(
				'max_contacts'	=> 2
				,'mailboxes'	=> $anexo
				,'remove_existing' => 'yes'
			);
			$this->model->updateData('ps_aors', array('id' => $anexo), $ps_aors);
			$ps_endpoints = array(
				'context'	=> $context
				,'disallow'	=> 'all'
				,'allow'	=> $codecs
				,'direct_media'	=> 'no'
				,'from_user'	=> $anexo
				,'rtp_symmetric'	=> 'yes'
				,'send_rpid'	=> 'yes'
				,'allow_subscribe'	=> 'yes'
				,'rewrite_contact'	=> 'no'
				,'dtmf_mode'	=> 'info'
				,'mailboxes'	=> $anexo.'@'.$context
				,'aggregate_mwi'	=> 'yes'
				,'allow_subscribe' => 'yes'
			);
			$this->model->updateData('ps_endpoints', array('id' => $anexo), $ps_endpoints);
			pbxRegeneratePJSip();
			header('Location: ' . base_url() . $this->_moduleUrl . 'anexoShow/' . $anexoId);
			exit();
		}
		#GET ANEXO BY ID
		$anexo = $this->model->getDataRow('ps_endpoints', array('id' => $anexoId));
		/**
		 * BODY
		 */
		$_a = $this->model->getDataRow('ps_auths', array('id' => $anexo->id));
		$_e = $this->model->getDataRow('ps_aors', array('id' => $anexo->id));
		$contexts = $this->model->getData('context');
		$context_options = array();
		foreach($contexts as $k) {
			$context_options[$k->context_name] = $k->context_name;
		}
		$this->_data['CONTEXT_SELECT'] = form_dropdown('context', $context_options, $anexo->context, 'class="form-control"');
		$this->_data['TITLE_BODY'] = "ANEXO";
		$this->_data['ANEXO'] = $_a->username;
		$this->_data['PASSWORD'] = $_a->password;
		$this->_data['CODECS'] = $anexo->allow;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/anexo/anexo-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Anexo';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Anexo ' . $anexo->id;
		generatePage($this->_body);
	} 
	/**
	 * function anexoDel
	 * Delete a Anexo
	 */
	public function anexoDel() {
		$anexoId = $this->uri->segment(4, TRUE);
		$anexoId = filter_var($anexoId,FILTER_VALIDATE_INT) ? $anexoId : 0;
		if($anexoId == 0x0000 || !$this->my_acl->acceso('pbx_anexo_delete') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET ANEXO BY ID
		$anexo = $this->model->getDataRow('ps_endpoints', array('id' => $anexoId));
		if($_POST) {
			$anexoDel = $this->security->xss_clean($this->input->post('detele_id'));
			if($anexoId != $anexoDel) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_anexo_delete')) {
				$this->model->deleteData('ps_auths', array('id' => $anexoDel));
				$this->model->deleteData('ps_aors', array('id' => $anexoDel));
				$this->model->deleteData('ps_endpoints', array('id' => $anexoDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $anexo->id;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'ANEXO';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar ANEXO';
		generatePage($this->_body);
	}
	
}