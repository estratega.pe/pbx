<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provider extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/provider/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_provider_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function provider
	 * List all providers
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_provider_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET PROVIDERS
		$sips = $this->model->getData('ps_registrations');
		$sip = array();
		foreach($sips  as $k) {
			$show = (!$this->my_acl->acceso('pbx_provider_show')) ? '' : anchor(base_url() . $this->_moduleUrl . 'providerShow/' . $k->id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('pbx_provider_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'providerEdit/' . $k->id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = (!$this->my_acl->acceso('pbx_provider_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'providerDel/' . $k->id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($sip, array(
				'PROVIDER_NAME'		=> $k->id
				,'PROVIDER_CLIENT'	=> $k->client_uri
				,'PROVIDER_SERVER'	=> $k->server_uri
				,'PROVIDER_TRANSPORT'	=> $k->transport
				,'ACTIONS'	=> $show
				. $edit
				. $delete
			));
		}
		$this->_data['PROVIDER_ITEM'] = $sip;
		$this->_data['TITLE_BODY'] = "SIP";
		$this->_data['LINK_PROVIDER_ADD'] = (!$this->my_acl->acceso('pbx_provider_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'providerAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$body = $this->parser->parse('pbx/provider/provider', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'SIP';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de SIP';
		generatePage($this->_body);
	}
	
	/**
	 * function providerAdd
	 * Add new Provider
	 */
	public function providerAdd() {
		if(!$this->my_acl->acceso('pbx_provider_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$username = $this->security->xss_clean($this->input->post('username', TRUE));
			$password = $this->security->xss_clean($this->input->post('password', TRUE));
			$provider = $this->security->xss_clean($this->input->post('provider', TRUE));
			$port = $this->security->xss_clean($this->input->post('port', TRUE));
			$context = $this->security->xss_clean($this->input->post('context', TRUE));
			$codecs = $this->security->xss_clean($this->input->post('codecs', TRUE));
			$codecs = trim(str_replace(' ','',$codecs));
			if(empty($username) || empty($password) || empty($provider) || empty($port) || empty($context) || empty($codecs) ) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'providerAdd');
				exit();
			}
			//sip:USUARIO@PROVIDER:PORT
			$label = 'SIP_' . $username;
			$ps_registrations = array(
				'id'	=> $label
				,'auth_rejection_permanent'	=> 'no'
				,'client_uri'	=> 'sip:' . $username . '@' . $provider . ':' . $port
				,'expiration'	=> 3600
				,'max_retries'	=> 20
				,'outbound_auth'	=> $label
				,'retry_interval'	=> 30
				,'forbidden_retry_interval'	=> 300
				,'server_uri'	=> 'sip:' . $provider . ':' . $port
				,'transport'	=> 'transport-udp'
				,'support_path'	=> 'no'
			);
			$this->model->createData('ps_registrations', $ps_registrations);
			$ps_auths = array(
				'id'	=> $label
				,'auth_type'	=> 'userpass'
				,'password'	=> $password
				,'username'	=> $username
			);
			$this->model->createData('ps_auths', $ps_auths);
			$ps_aors = array(
				'id'	=> $label
				,'max_contacts'	=> 100
			);
			$this->model->createData('ps_aors', $ps_aors);
			$ps_endpoints = array(
				'id'	=> $label
				,'transport'	=> 'transport-udp'
				,'aors'	=> $label
				,'context'	=> $context
				,'disallow'	=> 'all'
				,'allow'	=> $codecs
				,'direct_media'	=> 'no'
				,'from_user'	=> $username
				,'outbound_auth'	=> $label
				,'rtp_symmetric'	=> 'yes'
				,'send_rpid'	=> 'yes'
				,'dtmf_mode'	=> 'auto'
			);
			$this->model->createData('ps_endpoints', $ps_endpoints);
			$ps_endpoint_id_ips = array(
				'id'	=> $label
				,'endpoint'	=> $label
				,'match'	=> $provider
			);
			$this->model->createData('ps_endpoint_id_ips', $ps_endpoint_id_ips);
			pbxRegeneratePJSip();
			header('Location: ' . base_url() . $this->_moduleUrl . 'providerShow/' . $label);
			exit();
		}
		/**
		 * BODY
		 */
		$contexts = $this->model->getData('context');
		$context_options = array();
		foreach($contexts as $k) {
			$context_options[$k->context_name] = $k->context_name;
		}
		$this->_data['CONTEXT_SELECT'] = form_dropdown('context', $context_options, 'X', 'class="form-control"');
		$auth_rejection_permanent = array('yes' => 'SI','no' => 'NO');
		$this->_data['SELECT_AUTH_REJECTION_PERMANENT'] = form_dropdown('auth_rejection_permanent', $auth_rejection_permanent, 'no', 'class="form-control"');
		$this->_data['TITLE_BODY'] = "SIP";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/provider/provider-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'SIP';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo proveedor SIP';
		generatePage($this->_body);
	}
	/**
	 * function providerShow
	 * Show a Provider
	 */
	public function providerShow() {
		$providerId = $this->uri->segment(4, TRUE);
		#GET PROVIDER BY ID
		$provider = $this->model->getDataRow('ps_registrations', array('id' => $providerId));
		$_p = explode(':', $provider->server_uri);
		$_s = explode('_',$provider->id);
		if($_s[0] != 'SIP' || !$this->my_acl->acceso('pbx_provider_show') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$_a = $this->model->getDataRow('ps_auths', array('id' => $provider->id));
		$_e = $this->model->getDataRow('ps_endpoints', array('id' => $provider->id));
		$providerIP = str_replace('SIP_','',$provider->id);
		$this->_data['TITLE_BODY'] = "SIP";
		$this->_data['USERNAME'] = $_a->username;
		$this->_data['PASSWORD'] = $_a->password;
		$this->_data['PROVIDER'] = $_p[1];
		$this->_data['PORT'] = $_p[2];
		$this->_data['CONTEXT'] = $_e->context;
		$this->_data['CODECS'] = $_e->allow;
		$body = $this->parser->parse('pbx/provider/provider-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'SIP';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver SIP ' . $provider->id;
		generatePage($this->_body);
	}
	/**
	 * function providerEdit
	 * Edit a Provider
	 */
	public function providerEdit() {
		$providerId = $this->uri->segment(4, TRUE);
		#GET PROVIDER BY ID
		$provider = $this->model->getDataRow('ps_registrations', array('id' => $providerId));
		$_p = explode(':', $provider->server_uri);
		$_s = explode('_',$provider->id);
		if($_s[0] != 'SIP' || !$this->my_acl->acceso('pbx_provider_edit') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$username = $this->security->xss_clean($this->input->post('username', TRUE));
			$password = $this->security->xss_clean($this->input->post('password', TRUE));
			$provider = $this->security->xss_clean($this->input->post('provider', TRUE));
			$port = $this->security->xss_clean($this->input->post('port', TRUE));
			$context = $this->security->xss_clean($this->input->post('context', TRUE));
			$codecs = $this->security->xss_clean($this->input->post('codecs', TRUE));
			$codecs = trim(str_replace(' ','',$codecs));
			if(empty($username) || empty($password) || empty($provider) || empty($port) || empty($context) || empty($codecs) ) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'providerAdd');
				exit();
			}
			//sip:USUARIO@PROVIDER:PORT
			$label = 'SIP_' . $username;
			$ps_registrations = array(
				'id'	=> $label
				,'auth_rejection_permanent'	=> 'no'
				,'client_uri'	=> 'sip:' . $username . '@' . $provider . ':' . $port
				,'expiration'	=> 3600
				,'max_retries'	=> 20
				,'outbound_auth'	=> $label
				,'retry_interval'	=> 30
				,'forbidden_retry_interval'	=> 300
				,'server_uri'	=> 'sip:' . $provider . ':' . $port
				,'transport'	=> 'transport-udp'
				,'support_path'	=> 'no'
			);
			$this->model->updateData('ps_registrations', array('id' => $label), $ps_registrations);
			$ps_auths = array(
				'id'	=> $label
				,'auth_type'	=> 'userpass'
				,'password'	=> $password
				,'username'	=> $username
			);
			$this->model->updateData('ps_auths', array('id' => $label), $ps_auths);
			$ps_aors = array(
				'id'	=> $label
				,'max_contacts'	=> 100
			);
			$this->model->updateData('ps_aors', array('id' => $label), $ps_aors);
			$ps_endpoints = array(
				'id'	=> $label
				,'transport'	=> 'transport-udp'
				,'aors'	=> $label
				,'context'	=> $context
				,'disallow'	=> 'all'
				,'allow'	=> $codecs
				,'direct_media'	=> 'no'
				,'from_user'	=> $username
				,'outbound_auth'	=> $label
				,'rtp_symmetric'	=> 'yes'
				,'send_rpid'	=> 'yes'
				,'dtmf_mode'	=> 'auto'
			);
			$this->model->updateData('ps_endpoints', array('id' => $label), $ps_endpoints);
			$ps_endpoint_id_ips = array(
				'id'	=> $label
				,'endpoint'	=> $label
				,'match'	=> $provider
			);
			$this->model->updateData('ps_endpoint_id_ips', array('id' => $label), $ps_endpoint_id_ips);
			pbxRegeneratePJSip();
			header('Location: ' . base_url() . $this->_moduleUrl . 'providerShow/' . $label);
			exit();
		}
		/**
		 * BODY
		 */
		$_a = $this->model->getDataRow('ps_auths', array('id' => $provider->id));
		$_e = $this->model->getDataRow('ps_endpoints', array('id' => $provider->id));
		$contexts = $this->model->getData('context');
		$context_options = array();
		foreach($contexts as $k) {
			$context_options[$k->context_name] = $k->context_name;
		}
		$this->_data['CONTEXT_SELECT'] = form_dropdown('context', $context_options, $_e->context, 'class="form-control"');
		$providerIP = str_replace('SIP_','',$provider->id);
		$this->_data['TITLE_BODY'] = "SIP";
		$this->_data['USERNAME'] = $_a->username;
		$this->_data['PASSWORD'] = $_a->password;
		$this->_data['PROVIDER'] = $_p[1];
		$this->_data['PORT'] = $_p[2];
		$this->_data['CODECS'] = $_e->allow;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/provider/provider-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'SIP';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar SIP ' . $provider->id;
		generatePage($this->_body);
	} 
	/**
	 * function providerDel
	 * Delete a Provider
	 */
	public function providerDel() {
		$providerId = $this->uri->segment(4, TRUE);
		#GET PROVIDER BY ID
		$provider = $this->model->getDataRow('ps_registrations', array('id' => $providerId));
		$_p = explode(':', $provider->server_uri);
		$_s = explode('_',$provider->id);
		if($_s[0] != 'SIP' || !$this->my_acl->acceso('pbx_provider_delete') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$providerDel = $this->security->xss_clean($this->input->post('detele_id'));
			if($providerId != $providerDel) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_provider_delete')) {
				$this->model->deleteData('ps_registrations', array('id' => $providerDel));
				$this->model->deleteData('ps_auths', array('id' => $providerDel));
				$this->model->deleteData('ps_aors', array('id' => $providerDel));
				$this->model->deleteData('ps_endpoints', array('id' => $providerDel));
				$this->model->deleteData('ps_endpoint_id_ips', array('id' => $providerDel));
			}
			pbxRegeneratePJSip();
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $providerId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'SIP';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar SIP';
		generatePage($this->_body);
	}
	
}