<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timbrado extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_optionsTypes = array(
		'1'		=> 'Todos a la Vez'
		,'2'	=> 'Segun el orden'
	);
	private $_moduleUrl = 'pbx/timbrado/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_timbrado_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function timbrado
	 * List all trimbrados
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_timbrado_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET TIMBRADOS
		$timbrados = $this->model->getData('grupo_timbrado');
		$timbrado = array();
		foreach($timbrados  as $k) {
			$show = (!$this->my_acl->acceso('pbx_timbrado_show')) ? '' : anchor(base_url() . $this->_moduleUrl . 'timbradoShow/' . $k->gt_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('pbx_timbrado_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'timbradoEdit/' . $k->gt_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = (!$this->my_acl->acceso('pbx_timbrado_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'timbradoDel/' . $k->gt_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($timbrado, array(
				'NAME'			=> $k->gt_name
				,'DID'			=> $k->gt_did
				,'ACTIONS'	=> $show
				. $edit
				. $delete
			));
		}
		$this->_data['TIMBRADO_ITEM'] = $timbrado;
		$this->_data['TITLE_BODY'] = "Timbrado";
		$this->_data['LINK_TIMBRADO_ADD'] = (!$this->my_acl->acceso('pbx_timbrado_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'timbradoAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$body = $this->parser->parse('pbx/timbrado/timbrado', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Timbrado';
		$this->_body['PAGE_DESCRIPTION'] = 'Lista de grupos de Timbrado';
		generatePage($this->_body);
	}
	
	/**
	 * function timbradoAdd
	 * Add new Timbrado
	 */
	public function timbradoAdd() {
		if(!$this->my_acl->acceso('pbx_timbrado_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$gt_type = $this->security->xss_clean($this->input->post('gt_type', TRUE));
			$gt_name = $this->security->xss_clean($this->input->post('gt_name', TRUE));
			$_gt_anexos = $this->security->xss_clean($this->input->post('gt_anexos', TRUE));
			$gt_did = $this->security->xss_clean($this->input->post('gt_did', TRUE));
			if(empty($gt_name) || empty($gt_type) || empty($gt_did) || empty($_gt_anexos) || !$this->my_acl->acceso('pbx_timbrado_add')) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'timbradoAdd');
				exit();
			}
			$this->model->createData('grupo_timbrado', array(
				'gt_name'		=> $gt_name
				,'gt_type'	=> $gt_type
				,'gt_did'		=> $gt_did
				,'gt_anexos'	=> implode(',',$_gt_anexos)
			));
			$newGT = $this->db->insert_id();
			pbxRegenerateRingGroup();
			header('Location: ' . base_url() . $this->_moduleUrl . 'timbradoShow/' . $newGT);
			exit();
		}
		/**
		 * BODY
		 */
		$anexos = $this->model->getData('ps_endpoints');
		$optionsAnexos = array();
		foreach($anexos as $a) {
			if(preg_match("/SIP_/", $a->id) || preg_match("/TRUNK_/", $a->id)) {
				continue;
			}
			$optionsAnexos[$a->id] = $a->id;
		}
		$this->_data['GT_TYPE'] 	= form_dropdown('gt_type', $this->_optionsTypes, (isset($gt_type)) ? $gt_type : 0x0001, 'required class="form-control" ');
		$this->_data['GT_ANEXOS'] 	= form_dropdown('gt_anexos[]', $optionsAnexos, (isset($_gt_anexos)) ? $_gt_anexos : array('x','y'), ' class="required multiselect" multiselect ');
		$this->_data['GT_DID'] = form_input('gt_did', (isset($gt_did)) ? $gt_did : '', 'required class="form-control" placeholder="Ingrese el numero de entrada o DID" ');
		$this->_data['GT_NAME'] = form_input('gt_name', (isset($gt_name)) ? $gt_name : '', 'required class="form-control" placeholder="Ingrese el nombre para el Grupo de Timbrado" ');
		
		$this->_data['TITLE_BODY'] = "Timbrado";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/timbrado/timbrado-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Timrbado';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Grupo de Timbrado';
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/multiselect/multiselect.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/multiselect/ui/ui.multiselect.js', 'bottom', 'file');
		addJS('$(".multiselect").multiselect();', 'bottom', 'inline');
		generatePage($this->_body);
	}
	/**
	 * function timbradoShow
	 * Show a Timbrado
	 */
	public function timbradoShow() {
		$gtId = $this->uri->segment(4);
		$gtId = filter_var($gtId,FILTER_VALIDATE_INT) ? $gtId : 0;
		if($gtId <= 0x0000) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET GRUPO TIMBRADO BY ID
		$gt = $this->model->getDataRow('grupo_timbrado', array('gt_id' => $gtId));
		/**
		 * BODY
		 */
		$anexos = $this->model->getData('ps_endpoints');
		$optionsAnexos = array();
		foreach($anexos as $a) {
			if(preg_match("/SIP_/", $a->id) || preg_match("/TRUNK_/", $a->id)) {
				continue;
			}
			$optionsAnexos[$a->id] = $a->id;
		}
		$anexos = explode(',', $gt->gt_anexos);
		$this->_data['GT_TYPE'] 	= form_dropdown('gt_type', $this->_optionsTypes, $gt->gt_type, 'required class="form-control" disabled');
		$this->_data['GT_ANEXOS'] 	= form_dropdown('gt_anexos[]', $optionsAnexos, $anexos, ' class="required multiselect" multiple disabled');
		$this->_data['GT_DID'] = form_input('gt_did', $gt->gt_did, 'required class="form-control" readonly ');
		$this->_data['GT_NAME'] = form_input('gt_name', $gt->gt_name, 'required class="form-control" readonly ');
		
		$this->_data['TITLE_BODY'] = "Timbrado";
		$body = $this->parser->parse('pbx/timbrado/timbrado-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Timbrado';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Timbrado ' . $gt->gt_name;
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/multiselect/multiselect.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/multiselect/ui/ui.multiselect.js', 'bottom', 'file');
		addJS('$(".multiselect").multiselect();', 'bottom', 'inline');
		generatePage($this->_body);
	}
	/**
	 * function timbradoEdit
	 * Edit a Timbrado
	 */
	public function timbradoEdit() {
		$gtId = $this->uri->segment(4);
		$gtId = filter_var($gtId,FILTER_VALIDATE_INT) ? $gtId : 0x0000;
		if($gtId < 0x0000 || !$this->my_acl->acceso('pbx_timbrado_edit') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET GRUPO TIMBRADO BY ID
		$gt = $this->model->getDataRow('grupo_timbrado', array('gt_id' => $gtId));
		if($_POST) {
			$gt_type = $this->security->xss_clean($this->input->post('gt_type', TRUE));
			$gt_name = $this->security->xss_clean($this->input->post('gt_name', TRUE));
			$_gt_anexos = $this->security->xss_clean($this->input->post('gt_anexos', TRUE));
			$gt_did = $this->security->xss_clean($this->input->post('gt_did', TRUE));
			if(empty($gt_name) || empty($gt_type) || empty($gt_did) || empty($_gt_anexos) || !$this->my_acl->acceso('pbx_timbrado_add')) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'timbradoAdd');
				exit();
			}
			$this->model->updateData('grupo_timbrado', array('gt_id' => $gtId), array(
				'gt_name'		=> $gt_name
				,'gt_type'	=> $gt_type
				,'gt_did'		=> $gt_did
				,'gt_anexos'	=> implode(',',$_gt_anexos)
			));
			pbxRegenerateRingGroup();
			header('Location: ' . base_url() . $this->_moduleUrl . 'timbradoShow/' . $gtId);
			exit();
		}
		/**
		 * BODY
		 */
		$anexos = $this->model->getData('ps_endpoints');
		$optionsAnexos = array();
		foreach($anexos as $a) {
			if(preg_match("/SIP_/", $a->id) || preg_match("/TRUNK_/", $a->id)) {
				continue;
			}
			$optionsAnexos[$a->id] = $a->id;
		}
		$anexos = explode(',', $gt->gt_anexos);
		$this->_data['GT_TYPE'] 	= form_dropdown('gt_type', $this->_optionsTypes, $gt->gt_type, 'required class="form-control" required');
		$this->_data['GT_ANEXOS'] 	= form_dropdown('gt_anexos[]', $optionsAnexos, $anexos, ' class="required multiselect" required multiple');
		$this->_data['GT_DID'] = form_input('gt_did', $gt->gt_did, 'required class="form-control" required ');
		$this->_data['GT_NAME'] = form_input('gt_name', $gt->gt_name, 'required class="form-control" required ');
		
		$this->_data['TITLE_BODY'] = "Timbrado";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/timbrado/timbrado-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Timbrado';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Timbrado';
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/multiselect/multiselect.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/multiselect/ui/ui.multiselect.js', 'bottom', 'file');
		addJS('$(".multiselect").multiselect();', 'bottom', 'inline');
		generatePage($this->_body);
	} 
	/**
	 * function areaDel
	 * Delete a Area
	 */
	public function timbradoDel() {
		$areaId = $this->uri->segment(4);
		$areaId = filter_var($areaId,FILTER_VALIDATE_INT) ? $areaId : 0;
		if($areaId < 0x0000 || !$this->my_acl->acceso('pbx_timbrado_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$areaDel = $this->security->xss_clean($this->input->post('detele_id'));
			$areaDel = filter_var($areaDel,FILTER_VALIDATE_INT) ? $areaDel : '';
			if(empty($areaDel) || $areaId != $areaDel) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_timbrado_delete')) {
				$this->model->deleteData('grupo_timbrado', array('gt_id' => $areaDel));
			}
			pbxRegenerateRingGroup();
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $areaId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Timbrado';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Grupo de Timbrado';
		generatePage($this->_body);
	}
	
}
