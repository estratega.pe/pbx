<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class International extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/international/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_international_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function international
	 * List all International
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_international_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET INTERNATIONAL
		$internationals = $this->model->getData('internationals');
		$international = array();
		foreach($internationals  as $k) {
			$show = (!$this->my_acl->acceso('pbx_international_show')) ? '' : anchor(base_url() . $this->_moduleUrl . 'internationalShow/' . $k->int_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('pbx_international_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'internationalEdit/' . $k->int_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = (!$this->my_acl->acceso('pbx_international_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'internationalDel/' . $k->int_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($international, array(
				'PAIS'			=> $k->int_name
				,'CODE'			=> $k->int_code
				,'ACTIONS'	=> $show
				. $edit
				. $delete
			));
		}
		$this->_data['INTERNATIONAL_ITEM'] = $international;
		$this->_data['TITLE_BODY'] = "Internacional";
		$this->_data['LINK_INTERNATIONAL_ADD'] = (!$this->my_acl->acceso('pbx_international_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'internationalAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$body = $this->parser->parse('pbx/international/international', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Internacionales';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de Internacionales';
		generatePage($this->_body);
	}
	
	/**
	 * function internationalAdd
	 * Add new International
	 */
	public function internationalAdd() {
		if(!$this->my_acl->acceso('pbx_international_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$int_name = $this->security->xss_clean($this->input->post('int_name', TRUE));
			$int_code = $this->security->xss_clean($this->input->post('int_code', TRUE));
			if(empty($int_name) || empty($int_code)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'internatioanlAdd');
				exit();
			}
			$this->model->createData('internationals', array(
				'int_name'		=> $int_name
				,'int_code'		=> $int_code
			));
			$newInternational = $this->db->insert_id();
			header('Location: ' . base_url() . $this->_moduleUrl . 'internationalShow/' . $newInternational);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Internacional";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/international/international-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Internacional';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Pais';
		generatePage($this->_body);
	}
	/**
	 * function internationalShow
	 * Show a International
	 */
	public function internationalShow() {
		$internationalId = $this->uri->segment(4);
		$internationalId = filter_var($internationalId,FILTER_VALIDATE_INT) ? $internationalId : 0;
		if(!$this->my_acl->acceso('pbx_international_show') || $internationalId == 0x0000) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET INTERNATIONAL BY ID
		$international = $this->model->getDataRow('internationals', array('int_id' => $internationalId));
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Internacional";
		$this->_data['PAIS'] = $international->int_name;
		$this->_data['CODE'] = $international->int_code;
		$body = $this->parser->parse('pbx/international/international-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Internacionales';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Internacional ' . $international->int_name;
		generatePage($this->_body);
	}
	/**
	 * function internationalEdit
	 * Edit a International
	 */
	public function internationalEdit() {
		$internationalId = $this->uri->segment(4);
		$internationalId = filter_var($internationalId,FILTER_VALIDATE_INT) ? $internationalId : 0x0000;
		if($internationalId < 0x0001 || !$this->my_acl->acceso('pbx_international_edit') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET INTERNATIONAL BY ID
		$international = $this->model->getDataRow('internationals', array('int_id' => $internationalId));
		if($_POST) {
			$int_name = $this->security->xss_clean($this->input->post('int_name', TRUE));
			$int_code = $this->security->xss_clean($this->input->post('int_code', TRUE));
			$int_id = $this->security->xss_clean($this->input->post('int_id', TRUE));
			if(empty($int_name) || $int_id != $internationalId || empty($int_code)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'internationalEdit/' . $internationalId);
				exit();
			}
			$this->model->updateData('internationals', array('int_id' => $internationalId),array(
				'int_name'	=> $int_name
				,'int_code'	=> $int_code
			));
			header('Location: ' . base_url() . $this->_moduleUrl . 'internationalShow/' . $internationalId);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['PAIS'] = $international->int_name;
		$this->_data['CODE'] = $international->int_code;
		$this->_data['INT_ID'] = $international->int_id;
		$this->_data['TITLE_BODY'] = "Internacional";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/international/international-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Internacionales';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Internacional';
		generatePage($this->_body);
	} 
	/**
	 * function internationalDel
	 * Delete a International
	 */
	public function internationalDel() {
		$internationalId = $this->uri->segment(4);
		$internationalId = filter_var($internationalId,FILTER_VALIDATE_INT) ? $internationalId : 0;
		if(!$this->my_acl->acceso('pbx_international_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$internationalDel = $this->security->xss_clean($this->input->post('detele_id'));
			$internationalDel = filter_var($internationalDel,FILTER_VALIDATE_INT) ? $internationalDel : '';
			if(empty($internationalDel) || $internationalDel != $internationalDel) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_international_delete')) {
				$this->model->deleteData('internationals', array('int_id' => $internationalDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $internationalId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Internacionales';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Internacional';
		generatePage($this->_body);
	}
	
}
