<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/user/';
	static $_rowsPage = 10;
	
	private $_typeDesvios = array(
		'0' => 'Ninguno'
		,'1'=> 'Anexos'
		,'2'=> 'Anexos y Locales'
		,'3'=> 'Anexos y Moviles'
		,'4'=> 'Locales y Moviles'
		,'5'=> 'Anexos, Locales y Moviles'
		,'6'=> 'Anexos, Locales, Moviles y Nacionales'
		,'7'=> 'Anexos, Locales, Moviles, Nacionales e Internacionales'
	);
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_user_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function user
	 * List all users
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_user_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET USERS
		$users = $this->model->getData('permisos');
		$user = array();
		if(count($users) >= 0x0001) {
			foreach($users  as $u) {
				$k = $this->model->getDataRow('user', array('user_id' => $u->id));
				$show = (!$this->my_acl->acceso('pbx_user_show')) ? '' : anchor(base_url() . $this->_moduleUrl .'userShow/' . $k->user_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
				$edit = (!$this->my_acl->acceso('pbx_user_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'userEdit/' . $k->user_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
				$del = (!$this->my_acl->acceso('pbx_user_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'userDel/' . $k->user_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"') . ' ';
				$del = ($k->user_id == 0x0001) ? '' : $del;
				array_push($user, array(
					'USERNAME'			=> $k->username
					,'FIRST_NAME'		=> $k->first_name
					,'LAST_NAME'		=> $k->last_name
					,'EMAIL'				=> $k->email
					,'ANEXO'				=> ($u->anexo == 0x0000) ? '' : $u->anexo
					,'STATUS_CLASS'	=> ($k->status == '1') ? 'success' : 'danger'
					,'STATUS_TEXT'	=> ($k->status == '1') ? 'ACTIVO' : 'INACTIVO'
					,'ACTIVE_CLASS'	=> ($k->active == '1') ? 'success' : 'danger'
					,'ACTIVE_TEXT'	=> ($k->active == '1') ? 'SI' : 'NO'
					,'ACTIONS'			=> $show
					. $edit
					. $del
				));
			}
		}
		$this->_data['USER_ITEM'] = $user;
		$this->_data['LINK_USER_ADD'] = (!$this->my_acl->acceso('pbx_user_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'userAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$this->_data['TITLE_BODY'] = "Usuario";
		$body = $this->parser->parse('pbx/user/user', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de usuarios';
		generatePage($this->_body);
	}
	
	/**
	 * function userAdd
	 * Add new User
	 */
	public function userAdd() {
		if(!$this->my_acl->acceso('pbx_user_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * HEADER
		 */
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/multiselect/multiselect.css', 'top', 'file');
		/**
		 * BODY
		 */
		if($_POST) {
			#Usuario
			$username = $this->security->xss_clean($this->input->post('pbx_user_username', TRUE));
			//$clave = $this->security->xss_clean($this->input->post('pbx_user_callpassword', TRUE));
			$first_name = $this->security->xss_clean($this->input->post('pbx_user_name', TRUE));
			$last_name = $this->security->xss_clean($this->input->post('pbx_user_lastname', TRUE));
			$email = $this->security->xss_clean($this->input->post('email', TRUE));
			$role = $this->security->xss_clean($this->input->post('role'));
			$active = $this->security->xss_clean($this->input->post('pbx_user_status'));
			$status = $this->security->xss_clean($this->input->post('pbx_user_status'));
			#PBX - Usuario
			$grupo = $this->security->xss_clean($this->input->post('pbx_user_areaselect', TRUE));
			$grupo_admin = $this->security->xss_clean($this->input->post('pbx_user_areaselect_admin', TRUE));
			$anexo = $this->security->xss_clean($this->input->post('pbx_user_anexoselect', TRUE));
			$dnd_can = $this->security->xss_clean($this->input->post('pbx_user_nomolestar', TRUE));
			$desvio_can = $this->security->xss_clean($this->input->post('pbx_user_desviollamadas', TRUE));
			$desvio_tipo = $this->security->xss_clean($this->input->post('pbx_user_tipodesviollamadas', TRUE));
			$jefesecretaria = $this->security->xss_clean($this->input->post('jefesecretaria', TRUE));
			$secretaria = $this->security->xss_clean($this->input->post('secretaria', TRUE));
			$secretaria = ($secretaria == 'X') ? '0' : $secretaria;
			$local_can = $this->security->xss_clean($this->input->post('local', TRUE));
			$local_time_for_call = $this->security->xss_clean($this->input->post('local_time_for_call', TRUE));
			$local_time_for_month = $this->security->xss_clean($this->input->post('local_time_for_month', TRUE));
			$nacional_can = $this->security->xss_clean($this->input->post('nacional', TRUE));
			$nacional_time_for_call = $this->security->xss_clean($this->input->post('nacional_time_for_call', TRUE));
			$nacional_time_for_month = $this->security->xss_clean($this->input->post('nacional_time_for_month', TRUE));
			$movil_can = $this->security->xss_clean($this->input->post('movil', TRUE));
			$movil_time_for_call = $this->security->xss_clean($this->input->post('movil_time_for_call', TRUE));
			$movil_time_for_month = $this->security->xss_clean($this->input->post('movil_time_for_month', TRUE));
			$internacional_can = $this->security->xss_clean($this->input->post('internacional', TRUE));
			$internacional_time_for_call = $this->security->xss_clean($this->input->post('internacional_time_for_call', TRUE));
			$internacional_time_for_month = $this->security->xss_clean($this->input->post('internacional_time_for_month', TRUE));
			$int_code = $this->security->xss_clean($this->input->post('int_code', TRUE));
			
			$notes = $this->security->xss_clean($this->input->post('editor1', TRUE));
			
			$dataUser = array(
				'username'		=> $username
				,'first_name'	=> $first_name
				,'last_name'	=> $last_name
				,'email'			=> $email
				,'role'				=> $role
				,'status'			=> (empty($status)) ? '0' : '1'
				,'active'			=> (empty($active)) ? '0' : '1'
				,'password'		=> $this->my_crypto->password(123456)
				,'created'		=> $this->session->userdata('userID')
				,'created_at'	=> date('Y-m-d H:i:s')
			);
			
			$this->model->createData('user', $dataUser);
			$newUser = $this->db->insert_id();
			$dataUserPbx = array(
				'id'	=> $newUser
				,'pwd_text'	=> '123456'//$clave
				,'grupo'	=> $grupo
				,'grupo_admin'	=> ($grupo == 0x0001) ? $grupo_admin : '0'
				,'anexo'	=> ($anexo == 'X') ? '0' : $anexo
				,'dnd_can'	=> ($dnd_can == 0x0001) ? $dnd_can : '0'
				,'desvio_can'	=> ($desvio_can == 0x0001) ? $desvio_can : '0'
				,'desvio_tipo'	=> $desvio_tipo
				,'jefesecretaria'	=> ($jefesecretaria == 0x0001) ? $jefesecretaria : '0'
				,'secretaria'	=> ($jefesecretaria == 0x0001) ? $secretaria : '0'
				,'local_can'	=> ($local_can == 0x0001) ? $local_can : '0'
				,'local_time_for_call'	=> ($local_time_for_call >= 0x0001) ? ($local_time_for_call*60) : '0'
				,'local_time_for_month'	=> ($local_time_for_month >= 0x0001) ? ($local_time_for_month*60) : '0'
				,'nacional_can'	=> ($nacional_can == 0x0001) ? $nacional_can : '0'
				,'nacional_time_for_call'	=> ($nacional_time_for_call >= 0x0001) ? ($nacional_time_for_call*60) : '0'
				,'nacional_time_for_month'	=> ($nacional_time_for_month >= 0x0001) ? ($nacional_time_for_month*60) : '0'
				,'movil_can'	=> ($movil_can == 0x0001) ? $movil_can : '0'
				,'movil_time_for_call'	=> ($movil_time_for_call >= 0x0001) ? ($movil_time_for_call*60) : '0'
				,'movil_time_for_month'	=> ($movil_time_for_month >= 0x0001) ? ($movil_time_for_month*60) : '0'
				,'internacional_can'	=> ($internacional_can == 0x0001) ? $internacional_can : '0'
				,'internacional_time_for_call'	=> ($internacional_time_for_call >= 0x0001) ? ($internacional_time_for_call*60) : '0'
				,'internacional_time_for_month'	=> ($internacional_time_for_month >= 0x0001) ? ($internacional_time_for_month*60) : '0'
				,'notes'	=> $notes
			);
			if($int_code)
			{
				if(is_array($int_code))
				{
					$totale = count($int_code);
					for($i = 0;$i<$totale;$i++)
					{
						$int_int[$int_code[$i]] = $int_code[$i];
						$a = explode('-', $int_code[$i]);
						$int_codes[$a[0]] = $a[1];
						$int_country[$a[1]] = $a[0];
					}
				} else {
					$int_int[$int_code] = $int_code;
					$a = explode('-', $int_code);
					$int_codes[$a[0]] = $a[1];
					$int_country[$a[1]] = $a[0];
				}
				$dataUserPbx['int_codes'] = implode(',',$int_codes);
				$dataUserPbx['int_country'] = implode(',',$int_country);
				$dataUserPbx['int_int'] = implode(',', $int_int);
			}
			$this->model->createData('permisos', $dataUserPbx);
			redirect(base_url() . $this->_moduleUrl . 'userShow/' . $newUser, 'refresh');
			exit();
		}
		#Get and Set Areas
		$areas = $this->model->getData('area');
		$areaOption = array();
		$areaAdminOption = array();
		$areaAdminOption['X'] = 'Ninguno';
		foreach( $areas as $a) {
			$areaOption[$a->area_id] = $a->area_name;
			if($a->area_admin == 0)
				$areaAdminOption[$a->area_id] = $a->area_name;
		}
		$this->_data['AREA_SELECT'] = form_dropdown('pbx_user_areaselect', $areaOption, 'X', 'class="form-control"');
		$this->_data['AREA_SELECT_ADMIN'] = form_dropdown('pbx_user_areaselect_admin', $areaAdminOption, 'X', 'class="form-control"');
		#Get and Set Anexos
		$anexos = $this->model->getData('ps_endpoints');
		$anexoOption = array();
		$anexoOption['X'] = 'Ninguno';
		foreach($anexos as $an) {
			if(preg_match("/SIP_/", $an->id) || preg_match("/TRUNK_/", $an->id)) {
				continue;
			}
			$anexoOption[$an->id] = $an->id;
		}
		$this->_data['ANEXO_SELECT'] = form_dropdown('pbx_user_anexoselect', $anexoOption, 'X', 'class="form-control"');
		
		#Otros
		$this->_data['dnd'] = form_checkbox('pbx_user_nomolestar', '1', FALSE, ' class="flat-red" ');//no molestar
		$this->_data['s_desvio'] 	= form_checkbox('pbx_user_desviollamadas', '1', FALSE, ' id="s_desvio" class="flat-red" ');//desvio de llamadas
		$this->_data['t_desvio'] 	= form_dropdown('pbx_user_tipodesviollamadas', $this->_typeDesvios, 0, ' id="t_desvio" class="form-control" ');//tipos de desvios
		#Locales
		$this->_data['local'] = form_checkbox('local', '1', FALSE, ' class="flat-red" ');
		$this->_data['local_time_for_call'] = form_input('local_time_for_call', '', ' class="form-control" placeholder="Ingrese tiempo maximo de duracion de la llamada en minutos" ');
		$this->_data['local_time_for_month'] = form_input('local_time_for_month', '', ' class="form-control" placeholder="Ingrese bolsa de minutos mensuales" ');
		#Nacionales
		$this->_data['nacional'] = form_checkbox('nacional', '1', FALSE, ' class="flat-red" ');
		$this->_data['nacional_time_for_call'] = form_input('nacional_time_for_call', '', ' class="form-control" placeholder="Ingrese tiempo maximo de duracion de la llamada en minutos" ');
		$this->_data['nacional_time_for_month'] = form_input('nacional_time_for_month', '', ' class="form-control" placeholder="Ingrese bolsa de minutos mensuales" ');
		#Moviles
		$this->_data['movil'] = form_checkbox('movil', '1', FALSE, ' class="flat-red" ');
		$this->_data['movil_time_for_call'] = form_input('movil_time_for_call', '', ' class="form-control" placeholder="Ingrese tiempo maximo de duracion de la llamada en minutos" ');
		$this->_data['movil_time_for_month'] = form_input('movil_time_for_month', '', ' class="form-control" placeholder="Ingrese bolsa de minutos mensuales" ');
		#Internacionales
		$intD = $this->model->getData('internationals');
		$optionsInt = array();
		foreach($intD as $ri)
		{
			$optionsInt[$ri->int_id.'-'.$ri->int_code] = $ri->int_name;
		}
		$this->_data['internacional'] = form_checkbox('internacional', '1', FALSE, ' class="flat-red" ');
		$this->_data['internacional_time_for_call'] = form_input('internacional_time_for_call', '', ' class="form-control" placeholder="Ingrese tiempo maximo de duracion de la llamada en minutos" ');
		$this->_data['internacional_time_for_month'] = form_input('internacional_time_for_month', '', ' class="form-control" placeholder="Ingrese bolsa de minutos mensuales" ');
		$this->_data['internacional_codes'] 	= form_dropdown('int_code[]', $optionsInt, array('x','y'), ' class="multiselect"');
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/multiselect/ui/ui.multiselect.js', 'bottom', 'file');
		addJS('$(".multiselect").multiselect();', 'bottom', 'inline');
		#Secretaria
		$secretarias = $this->model->getDataWhere('permisos', array('jefesecretaria!=' => 1));
		$secretaria_options = array();
		$secretaria_options['X'] = 'Ninguno';
		foreach($secretarias as $s) {
			$secretaria_options[$s->id] = $this->model->getDataRow('user', array('user_id' => $s->id))->last_name .', ' . $this->model->getDataRow('user', array('user_id' => $s->id))->first_name;
		}
		$this->_data['SECRETARIA_SELECT'] = form_dropdown('secretaria', $secretaria_options, 'X', 'class="form-control"');
		#Roles
		$roles = $this->model->getDataWhere('role', array('role_id!=' => 1));
		$role_options = array();
		foreach($roles as $k) {
			$role_options[$k->role_id] = $k->role_name;
		}
		$this->_data['ROLE_SELECT'] = form_dropdown('role', $role_options, '3', 'class="form-control"');
		
		$this->_data['pbx_user_callpassword'] = form_input('pbx_user_callpassword', generatePassword(), ' class="form-control" readonly');
		
		$this->_data['jefesecretaria'] = form_checkbox('jefesecretaria', '1', FALSE, ' class="flat-red" ');
		$this->_data['pbx_user_status'] = form_checkbox('pbx_user_status', '1', TRUE, ' class="flat-red" ');
		$this->_data['email'] = form_input('email', '', ' class="form-control" placeholder="Ingrese email" required');
		$this->_data['TITLE_BODY'] = "Usuario";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/user/user-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Usuario';
		generatePage($this->_body);
	}
	/**
	 * function userShow
	 * Show a User
	 */
	public function userShow() {
		$userId = $this->uri->segment(4);
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0;
		if($userId <= 0x0001 || !$this->my_acl->acceso('pbx_user_show') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * HEADER
		 */
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/multiselect/multiselect.css', 'top', 'file');
		/**
		 * BODY
		 */
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/multiselect/ui/ui.multiselect.js', 'bottom', 'file');
		addJS('$(".multiselect").multiselect();', 'bottom', 'inline');
		#GET USER BY ID
		$u = $this->model->getDataRow('permisos', array('id' => $userId));
		$user = $this->model->getDataRow('user', array('user_id' => $u->id));
		
		#Internacionales
		$intD = $this->model->getData('internationals');
		$optionsInt = array();
		foreach($intD as $ri)
		{
			$optionsInt[$ri->int_id.'-'.$ri->int_code] = $ri->int_name;
		}
		$intSelected = explode(',', $u->int_int);
		$this->_data['internacional_codes'] 	= form_dropdown('int_code[]', $optionsInt, $intSelected, ' class="multiselect" disabled');
		$this->_data['NOTES_VALUE'] = $u->notes;
		$this->_data['pbx_user_name'] = form_input('pbx_user_name', $user->first_name, ' class="form-control" readonly');
		$this->_data['pbx_user_lastname'] = form_input('pbx_user_lastname', $user->last_name, ' class="form-control" readonly');
		$this->_data['email'] = form_input('email', $user->email, ' class="form-control" readonly');
		$this->_data['AREA_SELECT'] = form_input('AREA_SELECT', $this->model->getDataRow('area', array('area_id' => $u->grupo))->area_name, ' class="form-control" readonly');
		$this->_data['AREA_SELECT_ADMIN'] = form_input('AREA_SELECT_ADMIN', ($u->grupo_admin == 0x0000) ? '' : $this->model->getDataRow('area', array('area_id' => $u->grupo_admin))->area_name, ' class="form-control" readonly');
		$this->_data['dnd'] = ($u->dnd_can == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['s_desvio'] = ($u->desvio_can == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['t_desvio'] = form_dropdown('t_desvio', $this->_typeDesvios, $u->desvio_tipo, 'class="form-control" disabled');
		$this->_data['ANEXO_SELECT'] = form_input('ANEXO_SELECT', $u->anexo, ' class="form-control" readonly');
		//$this->_data['pbx_user_callpassword'] = form_input('pbx_user_callpassword', $u->pwd_text, ' class="form-control" readonly');
		$this->_data['local'] = ($u->local_can == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['local_time_for_call'] = form_input('local_time_for_call', ($u->local_time_for_call >= 60) ? $u->local_time_for_call/60 : $u->local_time_for_call, ' class="form-control" readonly');
		$this->_data['local_time_for_month'] = form_input('local_time_for_month', ($u->local_time_for_month >= 60) ? $u->local_time_for_month/60 : $u->local_time_for_month, ' class="form-control" readonly');
		$this->_data['nacional'] = ($u->local_can == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['nacional_time_for_call'] = form_input('nacional_time_for_call', ($u->nacional_time_for_call >= 60) ? $u->nacional_time_for_call/60 : $u->nacional_time_for_call, ' class="form-control" readonly');
		$this->_data['nacional_time_for_month'] = form_input('nacional_time_for_month', ($u->nacional_time_for_month >= 60) ? $u->nacional_time_for_month/60 : $u->nacional_time_for_month, ' class="form-control" readonly');
		$this->_data['movil'] = ($u->movil_can == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['movil_time_for_call'] = form_input('movil_time_for_call', ($u->movil_time_for_call >= 60) ? $u->movil_time_for_call/60 : $u->movil_time_for_call, ' class="form-control" readonly');
		$this->_data['movil_time_for_month'] = form_input('movil_time_for_month', ($u->movil_time_for_month >= 60) ? $u->movil_time_for_month/60 : $u->movil_time_for_month, ' class="form-control" readonly');
		$this->_data['internacional'] = ($u->internacional_can == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['internacional_time_for_call'] = form_input('internacional_time_for_call', ($u->internacional_time_for_call >= 60) ? $u->internacional_time_for_call/60 : $u->internacional_time_for_call, ' class="form-control" readonly');
		$this->_data['internacional_time_for_month'] = form_input('internacional_time_for_month', ($u->internacional_time_for_month >= 60) ? $u->internacional_time_for_month/60 : $u->internacional_time_for_month, ' class="form-control" readonly');
		
		$this->_data['pbx_user_status'] = ($user->status == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['jefesecretaria'] = ($u->jefesecretaria == 0x0001) ? 'check-square-o' : 'square-o';
		$this->_data['pbx_user_username'] = form_input('pbx_user_username', $user->username, ' class="form-control" readonly');
		$this->_data['SECRETARIA_SELECT'] = form_input('SECRETARIA_SELECT', ($u->secretaria == 0x0000)? '' : $this->model->getDataRow('user', array('user_id' => $u->secretaria))->username, ' class="form-control" readonly');
		$this->_data['ROLE_SELECT'] = form_input('ROLE_SELECT', $this->model->getDataRow('role', array('role_id' => $user->role))->role_name, ' class="form-control" readonly');
		$this->_data['TITLE_BODY'] = "Usuario";
		$body = $this->parser->parse('pbx/user/user-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Usuario ' . $user->username;
		generatePage($this->_body);
	}
	/**
	 * function userEdit
	 * Edit a User
	 */
	public function userEdit() {
		$userId = $this->uri->segment(4);
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0x0000;
		if($userId < 0x0001 || !$this->my_acl->acceso('pbx_user_edit')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET USER BY ID
		$u = $this->model->getDataRow('permisos', array('id' => $userId));
		if($u == '') {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		$user = $this->model->getDataRow('user', array('user_id' => $u->id));
		/**
		 * HEADER
		 */
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/multiselect/multiselect.css', 'top', 'file');
		/**
		 * BODY
		 */
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/multiselect/ui/ui.multiselect.js', 'bottom', 'file');
		addJS('$(".multiselect").multiselect();', 'bottom', 'inline');
		if($_POST) {
			if($userId != $u->id) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			#Usuario
			$username = $this->security->xss_clean($this->input->post('pbx_user_username', TRUE));
			$clave = $this->security->xss_clean($this->input->post('pbx_user_callpassword', TRUE));
			$first_name = $this->security->xss_clean($this->input->post('pbx_user_name', TRUE));
			$last_name = $this->security->xss_clean($this->input->post('pbx_user_lastname', TRUE));
			$email = $this->security->xss_clean($this->input->post('email', TRUE));
			$role = $this->security->xss_clean($this->input->post('role'));
			$active = $this->security->xss_clean($this->input->post('pbx_user_status'));
			$status = $this->security->xss_clean($this->input->post('pbx_user_status'));
			#PBX - Usuario
			$grupo = $this->security->xss_clean($this->input->post('pbx_user_areaselect', TRUE));
			$grupo_admin = $this->security->xss_clean($this->input->post('pbx_user_areaselect_admin', TRUE));
			$anexo = $this->security->xss_clean($this->input->post('pbx_user_anexoselect', TRUE));
			$dnd_can = $this->security->xss_clean($this->input->post('pbx_user_nomolestar', TRUE));
			$desvio_can = $this->security->xss_clean($this->input->post('pbx_user_desviollamadas', TRUE));
			$desvio_tipo = $this->security->xss_clean($this->input->post('pbx_user_tipodesviollamadas', TRUE));
			$jefesecretaria = $this->security->xss_clean($this->input->post('jefesecretaria', TRUE));
			$secretaria = $this->security->xss_clean($this->input->post('secretaria', TRUE));
			$secretaria = ($secretaria == 'X') ? '0' : $secretaria;
			$local_can = $this->security->xss_clean($this->input->post('local', TRUE));
			$local_time_for_call = $this->security->xss_clean($this->input->post('local_time_for_call', TRUE));
			$local_time_for_month = $this->security->xss_clean($this->input->post('local_time_for_month', TRUE));
			$nacional_can = $this->security->xss_clean($this->input->post('nacional', TRUE));
			$nacional_time_for_call = $this->security->xss_clean($this->input->post('nacional_time_for_call', TRUE));
			$nacional_time_for_month = $this->security->xss_clean($this->input->post('nacional_time_for_month', TRUE));
			$movil_can = $this->security->xss_clean($this->input->post('movil', TRUE));
			$movil_time_for_call = $this->security->xss_clean($this->input->post('movil_time_for_call', TRUE));
			$movil_time_for_month = $this->security->xss_clean($this->input->post('movil_time_for_month', TRUE));
			$internacional_can = $this->security->xss_clean($this->input->post('internacional', TRUE));
			$internacional_time_for_call = $this->security->xss_clean($this->input->post('internacional_time_for_call', TRUE));
			$internacional_time_for_month = $this->security->xss_clean($this->input->post('internacional_time_for_month', TRUE));
			$int_code = $this->security->xss_clean($this->input->post('int_code', TRUE));
			
			$notes = $this->security->xss_clean($this->input->post('editor1', TRUE));
			
			$dataUser = array(
				'first_name'	=> $first_name
				,'last_name'	=> $last_name
				,'email'			=> $email
				,'role'				=> $role
				,'status'			=> (empty($status)) ? '0' : '1'
				,'active'			=> (empty($active)) ? '0' : '1'
				,'modified'		=> $this->session->userdata('userID')
				,'modified_at'	=> date('Y-m-d H:i:s')
			);
			
			$dataUserPbx = array(
				'grupo'	=> $grupo
				,'grupo_admin'	=> ($grupo == 0x0001) ? $grupo_admin : '0'
				,'anexo'	=> ($anexo == 'X') ? '0' : $anexo
				,'dnd_can'	=> ($dnd_can == 0x0001) ? $dnd_can : '0'
				,'desvio_can'	=> ($desvio_can == 0x0001) ? $desvio_can : '0'
				,'desvio_tipo'	=> $desvio_tipo
				,'jefesecretaria'	=> ($jefesecretaria == 0x0001) ? $jefesecretaria : '0'
				,'secretaria'	=> ($jefesecretaria == 0x0001) ? $secretaria : '0'
				,'local_can'	=> ($local_can == 0x0001) ? $local_can : '0'
				,'local_time_for_call'	=> ($local_time_for_call >= 0x0001) ? ($local_time_for_call*60) : '0'
				,'local_time_for_month'	=> ($local_time_for_month >= 0x0001) ? ($local_time_for_month*60) : '0'
				,'nacional_can'	=> ($nacional_can == 0x0001) ? $nacional_can : '0'
				,'nacional_time_for_call'	=> ($nacional_time_for_call >= 0x0001) ? ($nacional_time_for_call*60) : '0'
				,'nacional_time_for_month'	=> ($nacional_time_for_month >= 0x0001) ? ($nacional_time_for_month*60) : '0'
				,'movil_can'	=> ($movil_can == 0x0001) ? $movil_can : '0'
				,'movil_time_for_call'	=> ($movil_time_for_call >= 0x0001) ? ($movil_time_for_call*60) : '0'
				,'movil_time_for_month'	=> ($movil_time_for_month >= 0x0001) ? ($movil_time_for_month*60) : '0'
				,'internacional_can'	=> ($internacional_can == 0x0001) ? $internacional_can : '0'
				,'internacional_time_for_call'	=> ($internacional_time_for_call >= 0x0001) ? ($internacional_time_for_call*60) : '0'
				,'internacional_time_for_month'	=> ($internacional_time_for_month >= 0x0001) ? ($internacional_time_for_month*60) : '0'
				,'notes'	=> $notes
			);
			
			if($clave == 0x0001) {
				$newPwd = 123456;//generatePassword();
				$dataUser['password'] = $this->my_crypto->password($newPwd);
				$dataUserPbx['pwd_text'] = $newPwd;
			}
			
			if($int_code)
			{
				if(is_array($int_code))
				{
					$totale = count($int_code);
					for($i = 0;$i<$totale;$i++)
					{
						$int_int[$int_code[$i]] = $int_code[$i];
						$a = explode('-', $int_code[$i]);
						$int_codes[$a[0]] = $a[1];
						$int_country[$a[1]] = $a[0];
					}
				} else {
					$int_int[$int_code] = $int_code;
					$a = explode('-', $int_code);
					$int_codes[$a[0]] = $a[1];
					$int_country[$a[1]] = $a[0];
				}
				$dataUserPbx['int_codes'] = implode(',',$int_codes);
				$dataUserPbx['int_country'] = implode(',',$int_country);
				$dataUserPbx['int_int'] = implode(',', $int_int);
			}
			
			$this->model->updateData('user', array('user_id' => $u->id), $dataUser);
			
			$this->model->updateData('permisos', array('id' => $u->id), $dataUserPbx);
			
			redirect(base_url() . 'pbx/user/userShow/' . $u->id, 'refresh');
			exit();
		}
		#Internacionales
		$intD = $this->model->getData('internationals');
		$optionsInt = array();
		foreach($intD as $ri)
		{
			$optionsInt[$ri->int_id.'-'.$ri->int_code] = $ri->int_name;
		}
		$intSelected = explode(',', $u->int_int);
		//$intSelected['X'] = 'X'; 
		$this->_data['internacional_codes'] 	= form_dropdown('int_code[]', $optionsInt, $intSelected, ' class="multiselect" multiple');
		#Get and Set Areas
		$areas = $this->model->getData('area');
		$areaOption = array();
		$areaAdminOption = array();
		$areaAdminOption['X'] = 'Ninguno';
		foreach( $areas as $a) {
			$areaOption[$a->area_id] = $a->area_name;
			if($a->area_admin == 0)
				$areaAdminOption[$a->area_id] = $a->area_name;
		}
		$this->_data['AREA_SELECT'] = form_dropdown('pbx_user_areaselect', $areaOption, $u->grupo, 'class="form-control"');
		$this->_data['AREA_SELECT_ADMIN'] = form_dropdown('pbx_user_areaselect_admin', $areaAdminOption, ($u->grupo_admin == 0x0000) ? 'X' : $u->grupo_admin, 'class="form-control"');
		#Get and Set Anexos
		$anexos = $this->model->getData('ps_endpoints');
		$anexoOption = array();
		$anexoOption['X'] = 'Ninguno';
		foreach($anexos as $an) {
			if(preg_match("/SIP_/", $an->id) || preg_match("/TRUNK_/", $an->id)) {
				continue;
			}
			$anexoOption[$an->id] = $an->id;
		}
		$this->_data['ANEXO_SELECT'] = form_dropdown('pbx_user_anexoselect', $anexoOption, $u->anexo, 'class="form-control"');
		#Secretaria
		$secretarias = $this->model->getDataWhere('permisos', array('jefesecretaria!=' => 1));
		$secretaria_options = array();
		$secretaria_options['X'] = 'Ninguno';
		foreach($secretarias as $s) {
			$secretaria_options[$s->id] = $this->model->getDataRow('user', array('user_id' => $s->id))->last_name .', ' . $this->model->getDataRow('user', array('user_id' => $s->id))->first_name;
		}
		$this->_data['SECRETARIA_SELECT'] = form_dropdown('secretaria', $secretaria_options, $u->secretaria, 'class="form-control"');
		#Roles
		$roles = $this->model->getDataWhere('role', array('role_id!=' => 1));
		$role_options = array();
		foreach($roles as $k) {
			$role_options[$k->role_id] = $k->role_name;
		}
		$this->_data['ROLE_SELECT'] = form_dropdown('role', $role_options, $user->role, 'class="form-control"');
		
		$this->_data['NOTES_VALUE'] = $u->notes;
		$this->_data['pbx_user_name'] = form_input('pbx_user_name', $user->first_name, ' class="form-control" ');
		$this->_data['pbx_user_lastname'] = form_input('pbx_user_lastname', $user->last_name, ' class="form-control" ');
		$this->_data['email'] = form_input('email', $user->email, ' class="form-control" ');
		$this->_data['dnd'] = form_checkbox('pbx_user_nomolestar', '1', ($u->dnd_can == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['s_desvio'] = form_checkbox('pbx_user_desviollamadas', '1', ($u->desvio_can == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['t_desvio'] = form_dropdown('pbx_user_tipodesviollamadas', $this->_typeDesvios, $u->desvio_tipo, 'class="form-control" ');
		$this->_data['pbx_user_callpassword_text'] = form_input('pbx_user_callpassword_text', $u->pwd_text, ' class="form-control" readonly');
		$this->_data['pbx_user_callpassword'] = form_checkbox('pbx_user_callpassword', '1', FALSE, ' class="flat-red" ');
		$this->_data['local'] = form_checkbox('local', '1', ($u->local_can == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['local_time_for_call'] = form_input('local_time_for_call', ($u->local_time_for_call >= 60) ? $u->local_time_for_call/60 : $u->local_time_for_call, ' class="form-control" ');
		$this->_data['local_time_for_month'] = form_input('local_time_for_month', ($u->local_time_for_month >= 60) ? $u->local_time_for_month/60 : $u->local_time_for_month, ' class="form-control" ');
		$this->_data['nacional'] = form_checkbox('nacional', '1', ($u->local_can == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['nacional_time_for_call'] = form_input('nacional_time_for_call', ($u->nacional_time_for_call >= 60) ? $u->nacional_time_for_call/60 : $u->nacional_time_for_call, ' class="form-control" ');
		$this->_data['nacional_time_for_month'] = form_input('nacional_time_for_month', ($u->nacional_time_for_month >= 60) ? $u->nacional_time_for_month/60 : $u->nacional_time_for_month, ' class="form-control" ');
		$this->_data['movil'] = form_checkbox('movil', '1', ($u->movil_can == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['movil_time_for_call'] = form_input('movil_time_for_call', ($u->movil_time_for_call >= 60) ? $u->movil_time_for_call/60 : $u->movil_time_for_call, ' class="form-control" ');
		$this->_data['movil_time_for_month'] = form_input('movil_time_for_month', ($u->movil_time_for_month >= 60) ? $u->movil_time_for_month/60 : $u->movil_time_for_month, ' class="form-control" ');
		$this->_data['internacional'] = form_checkbox('internacional', '1', ($u->internacional_can == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['internacional_time_for_call'] = form_input('internacional_time_for_call', ($u->internacional_time_for_call >= 60) ? $u->internacional_time_for_call/60 : $u->internacional_time_for_call, ' class="form-control" ');
		$this->_data['internacional_time_for_month'] = form_input('internacional_time_for_month', ($u->internacional_time_for_month >= 60) ? $u->internacional_time_for_month/60 : $u->internacional_time_for_month, ' class="form-control" ');
		
		$this->_data['pbx_user_status'] = form_checkbox('pbx_user_status', '1', ($user->status == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['jefesecretaria'] = form_checkbox('jefesecretaria', '1', ($u->jefesecretaria == 0x0001) ? TRUE : FALSE, ' class="flat-red" ');
		$this->_data['pbx_user_username'] = form_input('pbx_user_username', $user->username, ' class="form-control" readonly');
		
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/user/user-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Usuario';
		generatePage($this->_body);
	} 
	/**
	 * function userDel
	 * Delete a User
	 */
	public function userDel() {
		$userId = $this->uri->segment(4);
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0;
		if($userId < 0x0002) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('pbx_user_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$userDel = $this->security->xss_clean($this->input->post('detele_id'));
			$userDel = filter_var($userDel,FILTER_VALIDATE_INT) ? $userDel : 0x0001;
			if($userId == 0x0001 || $userId != $userDel || $userDel == 0x0001) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_user_delete')) {
				$this->model->deleteData('permisos', array('id' => $userDel));
				$this->model->deleteData('user', array('user_id' => $userDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $userId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Usuario';
		generatePage($this->_body);
	}
	
}
