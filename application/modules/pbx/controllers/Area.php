<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/area/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_area_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function area
	 * List all areas
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_area_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET AREAS
		$areas = $this->model->getData('area');
		$area = array();
		foreach($areas  as $k) {
			$show = (!$this->my_acl->acceso('pbx_area_show')) ? '' : anchor(base_url() . $this->_moduleUrl . 'areaShow/' . $k->area_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('pbx_area_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'areaEdit/' . $k->area_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = (!$this->my_acl->acceso('pbx_area_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'areaDel/' . $k->area_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($area, array(
				'NAME'			=> $k->area_name
				,'ACTIONS'	=> $show
				. $edit
				. $delete
			));
		}
		$this->_data['AREA_ITEM'] = $area;
		$this->_data['TITLE_BODY'] = "Area";
		$this->_data['LINK_AREA_ADD'] = (!$this->my_acl->acceso('pbx_area_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'areaAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$body = $this->parser->parse('pbx/area/area', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Areas';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de areas';
		generatePage($this->_body);
	}
	
	/**
	 * function areaAdd
	 * Add new Area
	 */
	public function areaAdd() {
		if(!$this->my_acl->acceso('pbx_area_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$area_name = $this->security->xss_clean($this->input->post('area_name', TRUE));
			if(empty($area_name)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'areaAdd');
				exit();
			}
			$this->model->createData('area', array(
				'area_name'		=> $area_name
			));
			$newArea = $this->db->insert_id();
			header('Location: ' . base_url() . $this->_moduleUrl . 'areaShow/' . $newArea);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Area";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/area/area-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Areas';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nueva Area';
		generatePage($this->_body);
	}
	/**
	 * function areaShow
	 * Show a Area
	 */
	public function areaShow() {
		$areaId = $this->uri->segment(4);
		$areaId = filter_var($areaId,FILTER_VALIDATE_INT) ? $areaId : 0;
		#GET AREA BY ID
		$area = $this->model->getDataRow('area', array('area_id' => $areaId));
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Area";
		$this->_data['AREA_NAME'] = $area->area_name;
		$body = $this->parser->parse('pbx/area/area-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Areas';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Area ' . $area->area_name;
		generatePage($this->_body);
	}
	/**
	 * function areaEdit
	 * Edit a Area
	 */
	public function areaEdit() {
		$areaId = $this->uri->segment(4);
		$areaId = filter_var($areaId,FILTER_VALIDATE_INT) ? $areaId : 0x0000;
		if($areaId < 0x0001 || !$this->my_acl->acceso('pbx_area_edit') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET AREA BY ID
		$area = $this->model->getDataRow('area', array('area_id' => $areaId));
		if($_POST) {
			$area_name = $this->security->xss_clean($this->input->post('area_name', TRUE));
			if(empty($area_name)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'areaEdit/' . $areaId);
				exit();
			}
			$this->model->updateData('area', array('area_id' => $areaId),array(
				'area_name'		=> $area_name
			));
			header('Location: ' . base_url() . $this->_moduleUrl . 'areaShow/' . $areaId);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['NAME_VALUE'] = $area->area_name;
		$this->_data['TITLE_BODY'] = "Area";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/area/area-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Areas';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Area';
		generatePage($this->_body);
	} 
	/**
	 * function areaDel
	 * Delete a Area
	 */
	public function areaDel() {
		$areaId = $this->uri->segment(4);
		$areaId = filter_var($areaId,FILTER_VALIDATE_INT) ? $areaId : 0;
		if($areaId < 0x0001 || !$this->my_acl->acceso('pbx_area_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$areaDel = $this->security->xss_clean($this->input->post('detele_id'));
			$areaDel = filter_var($areaDel,FILTER_VALIDATE_INT) ? $areaDel : '';
			if(empty($areaDel) || $areaId != $areaDel) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_area_delete')) {
				$this->model->deleteData('area', array('area_id' => $areaDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $areaId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Areas';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Area';
		generatePage($this->_body);
	}
	
}