<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Context extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/context/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_context_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function context
	 * List all context
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_context_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET CONTEXTS
		$contexts = $this->model->getData('context');
		$context = array();
		foreach($contexts  as $k) {
			$show = (!$this->my_acl->acceso('pbx_context_show')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contextShow/' . $k->context_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('pbx_context_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contextEdit/' . $k->context_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = (!$this->my_acl->acceso('pbx_context_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contextDel/' . $k->context_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($context, array(
				'NAME'			=> $k->context_name
				,'ACTIONS'	=> $show
				. $edit
				. $delete
			));
		}
		$this->_data['CONTEXT_ITEM'] = $context;
		$this->_data['TITLE_BODY'] = "Contexto";
		$this->_data['LINK_CONTEXT_ADD'] = (!$this->my_acl->acceso('pbx_context_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contextAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$body = $this->parser->parse('pbx/context/context', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Contextos';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de contextos';
		generatePage($this->_body);
	}
	
	/**
	 * function contextAdd
	 * Add new Context
	 */
	public function contextAdd() {
		if(!$this->my_acl->acceso('pbx_context_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$context_name = $this->security->xss_clean($this->input->post('context_name', TRUE));
			if(empty($context_name)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'contextAdd');
				exit();
			}
			$this->model->createData('context', array(
				'context_name'		=> $context_name
			));
			$newContext = $this->db->insert_id();
			header('Location: ' . base_url() . $this->_moduleUrl . 'contextShow/' . $newContext);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Contexto";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/context/context-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Contextos';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Contexto';
		generatePage($this->_body);
	}
	/**
	 * function contextShow
	 * Show a Context
	 */
	public function contextShow() {
		$contextId = $this->uri->segment(4);
		$contextId = filter_var($contextId,FILTER_VALIDATE_INT) ? $contextId : 0;
		#GET CONTEXT BY ID
		$context = $this->model->getDataRow('context', array('context_id' => $contextId));
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Contexto";
		$this->_data['CONTEXT_NAME'] = $context->context_name;
		$body = $this->parser->parse('pbx/context/context-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Contextos';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Contexto ' . $context->context_name;
		generatePage($this->_body);
	}
	/**
	 * function contextEdit
	 * Edit a Context
	 */
	public function contextEdit() {
		$contextId = $this->uri->segment(4);
		$contextId = filter_var($contextId,FILTER_VALIDATE_INT) ? $contextId : 0x0000;
		if($contextId < 0x0001 || !$this->my_acl->acceso('pbx_context_edit') ) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET CONTEXT BY ID
		$context = $this->model->getDataRow('context', array('context_id' => $contextId));
		if($_POST) {
			$context_name = $this->security->xss_clean($this->input->post('context_name', TRUE));
			if(empty($context_name)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'contextEdit/' . $contextId);
				exit();
			}
			$this->model->updateData('context', array('context_id' => $areaId),array(
				'context_name'		=> $context_name
			));
			header('Location: ' . base_url() . $this->_moduleUrl . 'contextShow/' . $contextId);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['NAME_VALUE'] = $context->context_name;
		$this->_data['TITLE_BODY'] = "Contexto";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/context/context-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Contextos';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Context';
		generatePage($this->_body);
	} 
	/**
	 * function contextDel
	 * Delete a Context
	 */
	public function contextDel() {
		$contextId = $this->uri->segment(4);
		$contextId = filter_var($contextId,FILTER_VALIDATE_INT) ? $contextId : 0;
		if($contextId < 0x0001 || !$this->my_acl->acceso('pbx_context_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$contextDel = $this->security->xss_clean($this->input->post('detele_id'));
			$contextDel = filter_var($contextDel,FILTER_VALIDATE_INT) ? $contextDel : '';
			if(empty($contextDel) || $contextId != $contextDel) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_context_delete')) {
				$this->model->deleteData('context', array('context_id' => $contextDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $contextId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Contextos';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Contexto';
		generatePage($this->_body);
	}
	
}