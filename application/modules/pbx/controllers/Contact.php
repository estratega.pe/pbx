<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/contact/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('pbx_contact_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function contact
	 * List all contacts
	 */
	public function index() {
		if(!$this->my_acl->acceso('pbx_contact_show')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET CONTACTOS
		$contacts = $this->model->getData('contact');
		$contact = array();
		foreach($contacts  as $k) {
			$show = (!$this->my_acl->acceso('pbx_contact_show')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contactShow/' . $k->id_contact, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('pbx_contact_edit')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contactEdit/' . $k->id_contact, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = (!$this->my_acl->acceso('pbx_contact_delete')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contactDel/' . $k->id_contact, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($contact, array(
				'NAME'			=> $k->name_contact
				,'NUMBER'		=> $k->number_contact
				,'ACTIONS'	=> $show
				. $edit
				. $delete
			));
		}
		$this->_data['CONTACT_ITEM'] = $contact;
		$this->_data['TITLE_BODY'] = "Abonado";
		$this->_data['LINK_CONTACT_ADD'] = (!$this->my_acl->acceso('pbx_contact_add')) ? '' : anchor(base_url() . $this->_moduleUrl . 'contactAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$body = $this->parser->parse('pbx/contact/contact', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Abonados';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de abonados';
		generatePage($this->_body);
	}
	
	/**
	 * function contactAdd
	 * Add new Contact
	 */
	public function contactAdd() {
		if(!$this->my_acl->acceso('pbx_contact_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$name_contact = $this->security->xss_clean($this->input->post('contact_name', TRUE));
			$number_contact = $this->security->xss_clean($this->input->post('contact_number', TRUE));
			if(empty($name_contact) || empty($number_contact)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'contactAdd');
				exit();
			}
			$this->model->createData('contact', array(
				'name_contact'		=> $name_contact
				,'number_contact'	=> $number_contact
			));
			$newContact = $this->db->insert_id();
			header('Location: ' . base_url() . $this->_moduleUrl . 'contactShow/' . $newContact);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Abonado";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/contact/contact-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Abonados';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Abonado';
		generatePage($this->_body);
	}
	/**
	 * function contactShow
	 * Show a Contact
	 */
	public function contactShow() {
		$contactId = $this->uri->segment(4);
		$contactId = filter_var($contactId,FILTER_VALIDATE_INT) ? $contactId : 0;
		#GET CONTACT BY ID
		$contact = $this->model->getDataRow('contact', array('id_contact' => $contactId));
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Abonado";
		$this->_data['CONTACT_NAME'] = $contact->name_contact;
		$this->_data['CONTACT_NUMBER'] = $contact->number_contact;
		$body = $this->parser->parse('pbx/contact/contact-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Abonados';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Abonado ' . $contact->name_contact;
		generatePage($this->_body);
	}
	/**
	 * function contactEdit
	 * Edit a Contact
	 */
	public function contactEdit() {
		$contactId = $this->uri->segment(4);
		$contactId = filter_var($contactId,FILTER_VALIDATE_INT) ? $contactId : 0x0000;
		if($contactId < 0x0001) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('pbx_contact_edit')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET CONTACT BY ID
		$contact = $this->model->getDataRow('contact', array('id_contact' => $contactId));
		if($_POST) {
			$name_contact = $this->security->xss_clean($this->input->post('contact_name', TRUE));
			$number_contact = $this->security->xss_clean($this->input->post('contact_number', TRUE));
			if(empty($name_contact) || empty($number_contact)) {
				header('Location: ' . base_url() . $this->_moduleUrl . 'contactEdit/' . $contactId);
				exit();
			}
			$this->model->updateData('contact', array('id_contact' => $contactId),array(
				'name_contact'		=> $name_contact
				,'number_contact'	=> $number_contact
			));
			header('Location: ' . base_url() . $this->_moduleUrl . 'contactShow/' . $contactId);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['NAME_VALUE'] = $contact->name_contact;
		$this->_data['NUMBER_VALUE'] = $contact->number_contact;
		$this->_data['TITLE_BODY'] = "Abonado";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('pbx/contact/contact-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Abonados';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Abonado';
		generatePage($this->_body);
	} 
	/**
	 * function contactDel
	 * Delete a Contact
	 */
	public function contactDel() {
		$contactId = $this->uri->segment(4);
		$contactId = filter_var($contactId,FILTER_VALIDATE_INT) ? $contactId : 0;
		if($contactId < 0x0001) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('pbx_contact_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$contactDel = $this->security->xss_clean($this->input->post('detele_id'));
			$contactDel = filter_var($contactDel,FILTER_VALIDATE_INT) ? $contactDel : '';
			if(empty($contactDel) || $contactId != $contactDel) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_contact_delete')) {
				$this->model->deleteData('contact', array('id_contact' => $roleDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $contactId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Abonados';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Abonado';
		generatePage($this->_body);
	}
	
}