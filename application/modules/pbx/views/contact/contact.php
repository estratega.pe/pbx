					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  {LINK_CONTACT_ADD} 
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>NOMBRES</th>
                        <th>NUMERO</th>
												<th>ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {CONTACT_ITEM}
											<tr>
                        <td>{NAME}</td>
												<td>{NUMBER}</td>
												<td>{ACTIONS}</td>
                      </tr>
											{/CONTACT_ITEM}
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>NOMBRES</th>
                        <th>NUMERO</th>
												<th>ACCIONES</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>