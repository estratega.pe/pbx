					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <form method="post">
								<div class="box-body">
									<div class="form-group">
										<label for="exampleInputEmail1">Nombre del Contacto</label>
										<input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Ingrese nombre del Abonado" value="{NAME_VALUE}" required>
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Numero del Contacto</label>
										<input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Ingrese numero del Abonado" value="{NUMBER_VALUE}" required>
									</div>
                </div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
								</form>
              </div><!-- /.box -->
						</div>
					</div>