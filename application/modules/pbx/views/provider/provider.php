					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  {LINK_PROVIDER_ADD} 
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>PROVEEDOR</th>
												<th>CLIENT</th>
												<th>SERVER</th>
												<th>TRANSPORT</th>
												<th>ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {PROVIDER_ITEM}
											<tr>
                        <td>{PROVIDER_NAME}</td>
												<td>{PROVIDER_CLIENT}</td>
												<td>{PROVIDER_SERVER}</td>
												<td>{PROVIDER_TRANSPORT}</td>
												<td>{ACTIONS}</td>
                      </tr>
											{/PROVIDER_ITEM}
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>PROVEEDOR</th>
												<th>CLIENT</th>
												<th>SERVER</th>
												<th>TRANSPORT</th>
												<th>ACCIONES</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>