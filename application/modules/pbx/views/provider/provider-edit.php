					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div>
								<form method="post">
								<div class="box-body">
									<div class="form-group">
										<label for="username">USUARIO</label>
										<input type="text" class="form-control" value="{USERNAME}" id="username" name="username" placeholder="Ingrese Usuario" required>
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="password">CLAVE</label>
										<input type="text" class="form-control" value="{PASSWORD}" id="password" name="password" placeholder="Ingrese clave" required>
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="provider">PROVEEDOR</label>
										<input type="text" class="form-control" value="{PROVIDER}" id="provider" name="provider" placeholder="Ingrese IP/Dominio de proveedor" required>
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="port">PUERTO</label>
										<input type="text" class="form-control" value="{PORT}" id="port" name="port" value="5060" placeholder="Ingrese puerto" required>
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="context">CONTEXT</label>
										{CONTEXT_SELECT}
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="codecs">CODECS</label>
										<input type="text" class="form-control" value="{CODECS}" id="codecs" name="codecs" placeholder="Ingrese codecs (gsm,ulaw,alaw,g729)" required>
									</div>
                </div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
								</form>
              </div><!-- /.box -->
						</div>
					</div>