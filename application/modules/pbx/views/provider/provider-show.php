					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="username">USUARIO</label>
										<input type="text" class="form-control" value="{USERNAME}" readonly>
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="password">CLAVE</label>
										<input type="text" class="form-control" value="{PASSWORD}" readonly>
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="provider">PROVEEDOR</label>
										<input type="text" class="form-control" value="{PROVIDER}" readonly>
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="port">PUERTO</label>
										<input type="text" class="form-control" value="{PORT}" readonly>
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="context">CONTEXT</label>
										<input type="text" class="form-control" value="{CONTEXT}" readonly>
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="codecs">CODECS</label>
										<input type="text" class="form-control" value="{CODECS}" readonly>
									</div>
                </div>
              </div><!-- /.box -->
						</div>
					</div>