					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <a href="{BASE_URL}admintask/permission/permissionAdd"><i class="fa fa-plus"></i></a> 
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>PERMISO</th>
												<th>KEY</th>
                        <th>ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {PERMISSION_ITEM}
											<tr>
                        <td>{PERMISSION}</td>
												<td>{KEY}</td>
												<td>{ACTIONS}</td>
                      </tr>
											{/PERMISSION_ITEM}
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>PERMISO</th>
												<th>KEY</th>
                        <th>ACCIONES</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>