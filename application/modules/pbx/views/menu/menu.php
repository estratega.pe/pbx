<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{BASE_URL}assets/plugins/Nestable/Nestable.css">
					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--<a href="{BASE_URL}admintask/menu/menuAdd"><i class="fa fa-plus"></i></a> -->
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="dd" id="menu_items">
										<ol class="dd-list">
											{MENU_ITEM}
												<li class="dd-item dd3-item" data-id="{ITEM_ID}">
														<div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">{ITEM_NAME}</div>
															<ol class="dd-list">
															{MENU_CHILDREN}
																<li class="dd-item dd3-item" data-id="{ITEM_ID}">
																		<div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">{ITEM_NAME}</div>
																</li>
															{/MENU_CHILDREN}
															</ol>
												</li>
											{/MENU_ITEM}
										</ol>
									</div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>