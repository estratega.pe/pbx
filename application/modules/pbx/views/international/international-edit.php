					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <form method="post">
								<div class="box-body">
									<div class="form-group">
										<label for="int_name">Nombre del Pais</label>
										<input type="text" class="form-control" id="int_name" name="int_name" placeholder="Ingrese Pais" value="{PAIS}" required>
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
										<input type="hidden" name="int_id" value="{INT_ID}" />
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_code">Codigo del Pais</label>
										<input type="text" class="form-control" id="int_code" name="int_code" placeholder="Codigo del Pais" value="{CODE}" required>
									</div>
                </div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
								</form>
              </div><!-- /.box -->
						</div>
					</div>