					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  {LINK_INTERNATIONAL_ADD} 
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>PAIS</th>
												<th>CODE</th>
												<th>ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {INTERNATIONAL_ITEM}
											<tr>
                        <td>{PAIS}</td>
												<td>{CODE}</td>
												<td>{ACTIONS}</td>
                      </tr>
											{/INTERNATIONAL_ITEM}
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>PAIS</th>
												<th>CODE</th>
												<th>ACCIONES</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>