					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-6">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">Detalles del usuario</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_name">Usuario</label>
										{pbx_user_username}
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_nomolestar">Rol</label>
										{ROLE_SELECT}
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_nomolestar">Usuario Activo</label>
										<i class="fa fa-{pbx_user_status}"></i>
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_nomolestar">Jefe-Secretaria</label>
										<i class="fa fa-{jefesecretaria}"></i>
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_nomolestar">Secretaria</label>
										{SECRETARIA_SELECT}
									</div>
								</div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_code">Nombres</label>
										{pbx_user_name}
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_code">Apellidos</label>
										{pbx_user_lastname}
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_nomolestar">E-mail</label>
										{email}
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_code">Grupo/Departamento</label>
										{AREA_SELECT}
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="area_select_admin">Administrador del Grupo/Departamento</label>
										{AREA_SELECT_ADMIN}
									</div>
                </div>
              </div><!-- /.box -->
							<div class="box">
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_nomolestar">No Molestar</label>
										<i class="fa fa-{dnd}"></i>
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_desviollamadas">Desvio de llamadas</label>
										<i class="fa fa-{s_desvio}"></i>
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_desviollamadas">Tipo Desvio de llamadas</label>
										{t_desvio}
									</div>
								</div>
								<div class="box box-warning"></div>
              </div><!-- /.box -->
						</div>
						<div class="col-xs-6">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">Detalles del Anexo</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_name">Anexo</label>
										{ANEXO_SELECT}
									</div>
                </div>
								<!--
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_callpassword">Clave de llamadas</label>
										{pbx_user_callpassword}
									</div>
                </div>
								-->
								<div class="box box-warning"></div>
								<div class="box-header">
									<h3 class="box-title">Permisos de llamadas</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_fijolocal">Fijo Local</label>
										<i class="fa fa-{local}"></i>
										{local_time_for_call}<br />
										{local_time_for_month}
									</div>
								</div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_fijonacional">Fijo Nacional</label>
										<i class="fa fa-{nacional}"></i>
										{nacional_time_for_call}<br />
										{nacional_time_for_month}
									</div>
								</div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_fijomovil">Celulares</label>
										<i class="fa fa-{movil}"></i>
										{movil_time_for_call}<br />
										{movil_time_for_month}
									</div>
								</div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_fijointernacional">Internacionales</label>
										<i class="fa fa-{internacional}"></i>
										{internacional_time_for_call}<br />
										{internacional_time_for_month}<br />
										{internacional_codes}
									</div>
								</div>
								<div class="box box-warning"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
              <div class="box">
								<div class="box-header">
									<h3 class="box-title">Nota</h3>
                </div>
								<div class="box-footer">
									<textarea id="editor1" name="editor1" rows="10" cols="80" disabled>{NOTES_VALUE}</textarea>
								</div>
							</div>
						</div>
					</div>