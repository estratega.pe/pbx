					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="gt_name">Nombre del Grupo de Timbrado</label>
										{GT_NAME}
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
									<div class="form-group">
										<label for="gt_did">DID</label>
										{GT_DID}
									</div>
									<div class="form-group">
										<label for="gt_type">Tipo de Timbrado</label>
										{GT_TYPE}
									</div>
									<div class="form-group">
										<label for="gt_anexos">Anexos</label>
										{GT_ANEXOS}
									</div>
                </div>
              </div><!-- /.box -->
						</div>
					</div>