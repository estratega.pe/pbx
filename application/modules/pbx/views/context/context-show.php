					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="context_name">Nombre del Contexto</label>
										<input type="text" class="form-control" value="{CONTEXT_NAME}" readonly>
									</div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>