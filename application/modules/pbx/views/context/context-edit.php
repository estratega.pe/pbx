					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <form method="post">
								<div class="box-body">
									<div class="form-group">
										<label for="context_name">Nombre del Contexto</label>
										<input type="text" class="form-control" id="context_name" name="context_name" placeholder="Ingrese nombre del Contexto" value="{NAME_VALUE}" required>
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
                </div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
								</form>
              </div><!-- /.box -->
						</div>
					</div>