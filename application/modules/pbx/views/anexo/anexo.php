					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  {LINK_ANEXO_ADD} 
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>ANEXO</th>
												<th>CONTEXTO</th>
												<th>CODECS</th>
												<th>ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {ANEXO_ITEM}
											<tr>
                        <td>{ANEXO}</td>
												<td>{CONTEXT}</td>
												<td>{CODECS}</td>
												<td>{ACTIONS}</td>
                      </tr>
											{/ANEXO_ITEM}
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>ANEXO</th>
												<th>CONTEXTO</th>
												<th>CODECS</th>
												<th>ACCIONES</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>