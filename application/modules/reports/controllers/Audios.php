﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audios extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'reports/salientes/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('reports_admin')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
		$this->load->helper('download');
	}
	
	public function index()
	{
		/**
		 * BODY
		 */
		if($_POST) {
			$line_select = $this->security->xss_clean($this->input->post('line_select', TRUE));
			$_fecha = $this->security->xss_clean($this->input->post('fecha', TRUE));
			if(!empty($_fecha)) {
				$fecha = explode('-', $_fecha);
				$date_start = trim($fecha[0]);
				$date_end = trim($fecha[1]);
				$fs = new DateTime(date('Y-m-d', strtotime($date_start)));
				$fe = new DateTime(date('Y-m-d', strtotime($date_end)));
			} else {
				$fs = new DateTime('today');
				$fe = new DateTime('today');
			}
			$preFolder = '/var/spool/asterisk/monitor/';
			$files = array();
			if($fs->format('Y-m-d') == $fe->format('Y-m-d')) {
				$folder = $preFolder . $line_select . '/' . $fe->format('Y/m/d');
				if(is_dir($folder) ) {
					$files[] = $folder;
				}
			} else {
				$folder = $preFolder . $line_select . '/' . $fs->format('Y/m/d');
				if(is_dir($folder) ) {
					$files[] = $folder;
				}
				do {
					$fs->modify('+1 day');
					$folder = $preFolder . $line_select . '/' . $fs->format('Y/m/d');
					if(is_dir($folder) ) {
						$files[] = $folder;
					}
				} while ( $fs->format('Y-m-d') != $fe->format('Y-m-d') );
			}
			if (count($files) >= 0x0001) {
				try {
					unlink(APPPATH . '../tmp/audios.tar.gz');
					$phar = new PharData(APPPATH . '../tmp/audios.tar');
					foreach($files as $k) {
						$phar->buildFromDirectory($k);
					}
					$phar->compress(Phar::GZ);
					unlink(APPPATH . '../tmp/audios.tar');
					force_download(APPPATH . '../tmp/audios.tar.gz', NULL);
				} catch (Exception $e) {
					echo "Exception : " . $e;
				}
			}
		}
		$typeLine = array(
			'inbound'	=> 'LLAMADAS ENTRANTES'
			,'outbound/local'	=> 'LLAMADAS SALIENTES LOCALES'
			,'outbound/movil'	=> 'LLAMADAS SALIENTES MOVILES'
			,'outbound/nacional'	=> 'LLAMADAS SALIENTES NACIONALES'
			,'outbound/internacional'	=> 'LLAMADAS SALIENTES INTERNACIONALES'
		);
		$this->_data['LINE_SELECT'] = form_dropdown('line_select', $typeLine, (isset($line_select)) ? $line_select : 'inbound', 'class="form-control"');
		$this->_data['FECHA_INPUT'] = form_input('fecha', (isset($_fecha)) ? $_fecha : '', ' class="form-control pull-right" id="reservation" ');
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('audios/audios', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'AUDIOS';
		$this->_body['PAGE_DESCRIPTION'] = 'Descargar Audios en bloques';
		addCSS($this->_baseUrl . 'assets/plugins/daterangepicker/daterangepicker-bs3.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/js/moment.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/daterangepicker/daterangepicker.js', 'bottom', 'file');
		addJS('$("#reservation").daterangepicker();
			$("#reservationtime").daterangepicker({timePicker: true, timePickerIncrement: 30, format: "DD/MM/YYYY h:mm A"});
		', 'bottom', 'inline');
		generatePage($this->_body);
	}
	
}