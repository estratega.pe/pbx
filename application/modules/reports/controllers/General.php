<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'pbx/area/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('reports_outbound_show')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function area
	 * List all areas
	 */
	public function index() {
		if(!$this->my_acl->acceso('reports_outbound_show')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		 //$this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='local' AND calldate > DATE_SUB(DATE(NOW()), INTERVAL 1 DAY)")->result();
		$ld = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='local' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate) = MONTH(NOW()) AND DAY(calldate) = DAY(NOW())")->result();
		$nd = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='nacional' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate) = MONTH(NOW()) AND DAY(calldate) = DAY(NOW())")->result();
		$md = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='movil' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate) = MONTH(NOW()) AND DAY(calldate) = DAY(NOW())")->result();
		$id = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='internacional' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate) = MONTH(NOW()) AND DAY(calldate) = DAY(NOW())")->result();
		$local_day = $ld[0]->Total;
		$nacional_day = $nd[0]->Total;
		$movil_day = $md[0]->Total;
		$internacional_day = $id[0]->Total;
		
		$lw = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='local' AND WEEKOFYEAR(calldate) = WEEKOFYEAR(NOW())")->result();
		$nw = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='nacional' AND WEEKOFYEAR(calldate) = WEEKOFYEAR(NOW())")->result();
		$mw = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='movil' AND WEEKOFYEAR(calldate) = WEEKOFYEAR(NOW())")->result();
		$iw = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='internacional' AND WEEKOFYEAR(calldate) = WEEKOFYEAR(NOW())")->result();
		$local_week = $lw[0]->Total;
		$nacional_week = $nw[0]->Total;
		$movil_week = $mw[0]->Total;
		$internacional_week = $iw[0]->Total;
		
		$lm = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='local' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate)=MONTH(NOW())")->result();
		$nm = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='nacional' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate)=MONTH(NOW())")->result();
		$mm = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='movil' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate)=MONTH(NOW())")->result();
		$im = $this->db->query("SELECT COUNT(*) as Total FROM cdr WHERE lastapp='Dial' AND type_dest='internacional' AND YEAR(calldate) = YEAR(NOW()) AND MONTH(calldate)=MONTH(NOW())")->result();
		$local_month = $lm[0]->Total;
		$nacional_month = $nm[0]->Total;
		$movil_month = $mm[0]->Total;
		$internacional_month = $im[0]->Total;
		
		$this->_data['TITLE_BODY'] = "Reportes";
		$body = $this->parser->parse('reports/general/general', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Reportes';
		$this->_body['PAGE_DESCRIPTION'] = 'Reportes Globales';
		addCSS($this->_baseUrl . 'assets/plugins/morris/morris.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/js/raphael-min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/morris/morris.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/fastclick/fastclick.min.js', 'bottom', 'file');
		addJS('var callday = new Morris.Donut({
          element: "call-day",
          resize: true,
          colors: ["#3c8dbc", "#f56954", "#00a65a", "#c0c0c0"],
          data: [
            {label: "Locales", value: '.$local_day.'},
            {label: "Nacionales", value: '.$nacional_day.'},
						{label: "Moviles", value: '.$movil_day.'},
            {label: "Internacionales", value: '.$internacional_day.'}
          ],
          hideHover: "auto"
        });
				var callweek = new Morris.Donut({
          element: "call-week",
          resize: true,
          colors: ["#3c8dbc", "#f56954", "#00a65a", "#c0c0c0"],
          data: [
            {label: "Locales", value: '.$local_week.'},
            {label: "Nacionales", value: '.$nacional_week.'},
						{label: "Moviles", value: '.$movil_week.'},
            {label: "Internacionales", value: '.$internacional_week.'}
          ],
          hideHover: "auto"
        });
				var callmonth = new Morris.Donut({
          element: "call-month",
          resize: true,
          colors: ["#3c8dbc", "#f56954", "#00a65a", "#c0c0c0"],
          data: [
            {label: "Locales", value: '.$local_month.'},
            {label: "Nacionales", value: '.$nacional_month.'},
						{label: "Moviles", value: '.$movil_month.'},
            {label: "Internacionales", value: '.$internacional_month.'}
          ],
          hideHover: "auto"
        });', 'bottom', 'inline');
		generatePage($this->_body);
	}	
}