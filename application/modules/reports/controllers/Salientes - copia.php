<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salientes extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'reports/salientes/';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('reports_outbound_show')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
		$this->load->helper('download');
	}
	
	public function index()
	{
		/**
		 * BODY
		 */
		$_limit = 50;
		$where = array(
			'type_call'			=> 'SALIENTE'
			,'disposition'	=> 'ANSWERED'
		);
		if($_POST) {
			$line_select = $this->security->xss_clean($this->input->post('line_select', TRUE));
			$call_select = $this->security->xss_clean($this->input->post('call_select', TRUE));
			$destino_input = $this->security->xss_clean($this->input->post('destino_input', TRUE));
			$troncal_select = $this->security->xss_clean($this->input->post('troncal_select', TRUE));
			$_fecha = $this->security->xss_clean($this->input->post('fecha', TRUE));
			$area_select = $this->security->xss_clean($this->input->post('area_select', TRUE));
			$excel = $this->security->xss_clean($this->input->post('excel', TRUE));
			if(!empty($_fecha)) {
				$fecha = explode('-', $_fecha);
				$date_start = trim($fecha[0]);
				$date_end = trim($fecha[1]);
				$where['calldate >='] = date('Y-m-d', strtotime($date_start)) . ' 00:00:00';
				$where['calldate <='] = date('Y-m-d', strtotime($date_end)) . ' 23:59:59';
			} else {
				$where['calldate >='] = date('Y-m-d') . ' 00:00:00';
				$where['calldate <='] = date('Y-m-d') . ' 23:59:59';
			}
			if($troncal_select != 'X') {
				if($line_select == 'ENTRANTE') {
					//$this->db->like('channel','PJSIP/'.$troncal_select);
					$where['dst'] = str_replace('SIP_','',$troncal_select);
				}
			}
			if(!empty($line_select)) {
				$where['type_call'] = $line_select;
			}
			if(!empty($destino_input)) {
				$where['dst'] = $destino_input;
			}
			if($call_select != 'X') {
				$where['type_dest'] = $call_select;
			}
			if($this->my_acl->acceso('admin_task') || $this->my_acl->acceso('reports_admin')) {
				if($area_select != 'X') {
					$where['grupo'] = $area_select;
				}
				if(!empty($user_select) && is_array($user_select)) {
					$this->db->where_in('usuario', $user_select);
				}
			} else {
				$where['usuario'] = $this->session->userdata('userID');
			}
			
			if($this->my_acl->acceso('admin_task') || $this->my_acl->acceso('reports_admin')) {
				
			} else {
				$where['usuario'] = $this->session->userdata('userID');
			}
		} else {
			//$where['usuario'] = $this->session->userdata('userID');
			$where['type_call'] = 'ENTRANTE';
			$where['calldate >='] = date('Y-m-d') . ' 00:00:00';
			$where['calldate <='] = date('Y-m-d') . ' 23:59:59';
			$this->db->limit(50);
		}
		$this->db->where($where);
		$this->db->order_by('calldate', 'desc');
		$calls = $this->db->get('cdr')->result();
		$call = array();
		foreach($calls as $c) {
			$contact = $this->model->getDataRow('contact', array('number_contact' => $c->dst));
			$u = $this->model->getDataRow('user', array('user_id' => $c->usuario));
			$_download = anchor(base_url() . $this->_moduleUrl . 'downloadAudio/' . $c->uniqueid, '<i class="fa fa-download"></i>', 'target="new" alt="Descargar"') . ' ';
			$troncal = explode('-',$c->channel);
			array_push($call, array(
				'FECHA'			=> date('d/m/Y H:i:s', strtotime($c->calldate))
				,'LINEA'		=> $c->type_call
				,'USUARIO'		=> ($u) ? $u->username : ''
				,'ORIGEN'		=> $c->src
				,'GRUPO'		=> ($c->grupo != 0x0000) ? $this->model->getDataRow('area', array('area_id' => $c->grupo))->area_name : ''
				,'DURACION'		=> gmdate('H:i:s', $c->billsec)
				,'DESTINO'		=> $c->dst
				,'ABONADO'		=> ($contact) ? $contact->name_contact : ''
				,'LLAMADA'		=> $c->type_dest
				,'TRONCAL'		=> str_replace('PJSIP/','',$troncal[0])
				,'AUDIO'		=> $_download
			));
		}
		$this->_data['CALLS'] = $call;
		
		$this->_data['TITLE_BODY'] = "Area";
		#Get and Set Areas
		if($this->my_acl->acceso('admin_task') || $this->my_acl->acceso('reports_admin')) {
			$areas = $this->model->getData('area');
			$users = $this->model->getData('permisos');
		} else {
			$areas = $this->model->getDataWhere('area', array('area_id' => $this->model->getDataRow('permisos', array('id' => $this->session->userdata('userID')))->grupo_admin));
			$users = $this->model->getDataWhere('permisos', array('grupo' => $this->model->getDataRow('permisos', array('id' => $this->session->userdata('userID')))->grupo_admin));
		}
		$areaOption = array();
		$areaOption['X'] = 'Seleccione...';
		$userOption = array();
		foreach( $areas as $a) {
			$areaOption[$a->area_id] = $a->area_name;
		}
		foreach( $users as $_u) {
			$userOption[$_u->id] = $this->model->getDataRow('user', array('user_id' => $_u->id))->username;
		}
		$troncales = $this->model->getData('ps_registrations');
		$troncalOption = array();
		foreach($troncales as $t) {
			$troncalOption[$t->id] = $t->id;
		}
		$tipoLlamada = array(
			'X'	=> 'Seleccione...'
			,'local'	=> 'LOCAL'
			,'nacional'	=> 'NACIONAL'
			,'movil'	=> 'MOVIL'
			,'internacional'	=> 'INTERNACIONAL'
		);
		$typeLine = array(
			'ENTRANTE'	=> 'Llamada Entrante'
			,'SALIENTE'	=> 'Llamada Saliente'
		);
		$this->_data['LINE_SELECT'] = form_dropdown('line_select', $typeLine, (isset($line_select)) ? $line_select : 'X', 'class="form-control"');
		$this->_data['CALL_SELECT'] = form_dropdown('call_select', $tipoLlamada, (isset($call_select)) ? $call_select : 'X', 'class="form-control"');
		$this->_data['AREA_SELECT'] = form_dropdown('area_select', $areaOption, (isset($area_select)) ? $area_select : 'X', 'class="form-control"');
		$this->_data['USER_SELECT'] = form_dropdown('user_select[]', $userOption, array('x','y'), ' class="multiselect" multiselect ');
		$this->_data['DESTINO_INPUT'] = form_input('destino_input', (isset($destino_input)) ? $destino_input : '', ' class="form-control" placeholder="Ingrese un numero de destino" ');
		$this->_data['FECHA_INPUT'] = form_input('fecha', (isset($_fecha)) ? $_fecha : '', ' class="form-control pull-right" id="reservation" ');
		$this->_data['TRONCAL_SELECT'] = form_dropdown('troncal_select', $troncalOption, (isset($troncal_select)) ? $troncal_select : 'X', 'class="form-control"');
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('salientes/salientes', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'REPORTES';
		$this->_body['PAGE_DESCRIPTION'] = 'Reporte de llamadas salientes';
		addCSS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/daterangepicker/daterangepicker-bs3.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/jquery.dataTables.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/js/moment.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.min.js', 'bottom', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addCSS($this->_baseUrl . 'assets/plugins/multiselect/multiselect.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/multiselect/ui/ui.multiselect.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/daterangepicker/daterangepicker.js', 'bottom', 'file');
		addJS('$(".multiselect").multiselect();', 'bottom', 'inline');
		addJS('$("#example2").DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"order": [[ 0, "desc" ]]
		});
			$("#reservation").daterangepicker();
			$("#reservationtime").daterangepicker({timePicker: true, timePickerIncrement: 30, format: "DD/MM/YYYY h:mm A"});
		', 'bottom', 'inline');
		if(isset($excel) && !empty($excel) && $excel == "EXCEL" ) {
			require_once (__DIR__.'/Classes/PHPExcel.php');
			$sLine = 1;
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("ConsultoresDePrimera.Com :: lgonzales")
									->setLastModifiedBy("ConsultoresDePrimera.Com :: lgonzales")
									->setTitle("d1erasi_pbx")
									->setSubject("d1erasi_pbx")
									->setDescription("d1erasi_pbx.")
									->setKeywords("d1erasi_pbx")
									->setCategory("ConsultoresDePrimera.Com");
			$objPHPExcel->setActiveSheetIndex(0)
									->setCellValue('A'.$sLine, 'Fecha')
									->setCellValue('B'.$sLine, 'Linea')
									->setCellValue('C'.$sLine, 'Usuario')
									->setCellValue('D'.$sLine, 'Grupo')
									->setCellValue('E'.$sLine, 'Duracion')
									->setCellValue('F'.$sLine, 'Origen')
									->setCellValue('G'.$sLine, 'Destino')
									->setCellValue('H'.$sLine, 'Abonado')
									->setCellValue('I'.$sLine, 'Llamada');
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('A')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('B')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('C')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('D')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('E')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('F')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('G')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('H')
									->setAutoSize(true);
			$objPHPExcel->getActiveSheet()
									->getColumnDimension('I')
									->setAutoSize(true);
			$sLine = $sLine +1;
			foreach($call as $k) {
				$objPHPExcel->setActiveSheetIndex(0)
										->setCellValue('A'.$sLine, $k['FECHA'])
										->setCellValue('B'.$sLine, $k['LINEA'])
										->setCellValue('C'.$sLine, $k['USUARIO'])
										->setCellValue('D'.$sLine, $k['GRUPO'])
										->setCellValue('E'.$sLine, $k['DURACION'])
										->setCellValue('F'.$sLine, $k['ORIGEN'])
										->setCellValue('G'.$sLine, $k['DESTINO'])
										->setCellValue('H'.$sLine, $k['ABONADO'])
										->setCellValue('I'.$sLine, $k['LLAMADA']);
				$sLine++;
			}
			$now = date('d-m-Y H-i-s');
			$objPHPExcel->getActiveSheet()->setTitle($now);
			$objPHPExcel->setActiveSheetIndex(0);
			header("Pragma: public");
			header("Cache-Control: private",false);
			header("Content-Type: application/force-download");
			header("Content-Description: File Transfer");
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Reporte generado el '.date('d/m/Y H:i:s').'.xls"');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			//header("Content-Type: application/zip");
			header("Content-Transfer-Encoding: binary");
			header('Cache-Control: max-age=0');
			ob_clean(); 
			flush();
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', true);
		}
		generatePage($this->_body);
	}
	
	public function downloadAudio() {
		$fileDownload = $this->uri->segment(4, TRUE);
		$audio = $this->model->getDataRow('cdr', array('uniqueid' => $fileDownload));
		if(count($audio) == 0x0001) {
			force_download('/var/spool/asterisk/monitor/' . $audio->audio, NULL);
		}
	}
	
}