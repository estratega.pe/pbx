					<form method="post">
					<div class="row">
						<div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="submit" class="btn btn-primary pull-right">DESCARGAR</button>
								</div>
							</div>
						</div>
					</div>
					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
                  <h3 class="box-title">TIPO LLAMADA</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
									{LINE_SELECT}
									<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
								</div>
								<div class="box-header with-border">
                  <h3 class="box-title">FECHAS</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      {FECHA_INPUT}
                    </div>
                  </div>
								</div>
							</div>
						</div>
					</div>
					</form>