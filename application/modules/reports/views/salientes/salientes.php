					<form method="post">
					<div class="row">
						<div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="submit" name="excel" value="EXCEL" class="btn btn-primary pull-left">REPORTE EXCEL</button>
									<button type="submit" class="btn btn-primary pull-right">GENERAR REPORTE</button>
								</div>
							</div>
						</div>
					</div>
					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-md-5">
							<div class="box box-primary">
								<div class="box-header with-border">
                  <h3 class="box-title">TRONCAL</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
									{TRONCAL_SELECT}
									</div>
								</div>
								<div class="box-header with-border">
                  <h3 class="box-title">TIPO LLAMADA</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
									{CALL_SELECT}
									</div>
								</div>
								<div class="box-header with-border">
                  <h3 class="box-title">GRUPO TIMBRADO</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
									{TIMBRADO_SELECT}
									</div>
								</div>
								<div class="box-header with-border">
                  <h3 class="box-title">FECHAS</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      {FECHA_INPUT}
                    </div>
                  </div>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="box box-primary">
								<div class="box-header with-border">
                  <h3 class="box-title">GRUPO/AREA</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
									{AREA_SELECT}
									</div>
								</div>
								<div class="box-header with-border">
                  <h3 class="box-title">USUARIO</h3>
                </div>
								<div class="box-body">
									<div class="form-group">
									{USER_SELECT}
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>FECHA</th>
						<th>LINEA</th>
						<th>USUARIO</th>
						<th>GRUPO</th>
						<th>DURACION</th>
						<th>ORIGEN</th>
						<th>DESTINO</th>
						<th>ABONADO</th>
						<th>LLAMADA</th>
						<th>AUDIO</th>
                      </tr>
                    </thead>
                    <tbody>
                    {CALLS}
						<tr>
							<td>{FECHA}</td>
							<td>{LINEA}</td>
							<td>{USUARIO}</td>
							<td>{GRUPO}</td>
							<td>{DURACION}</td>
							<td>{ORIGEN}</td>
							<td>{DESTINO}</td>
							<td>{ABONADO}</td>
							<td>{LLAMADA}</td>
							<td>{AUDIO}</td>
						</tr>
					{/CALLS}
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>FECHA</th>
						<th>LINEA</th>
						<th>USUARIO</th>
						<th>GRUPO</th>
						<th>DURACION</th>
						<th>ORIGEN</th>
						<th>DESTINO</th>
						<th>ABONADO</th>
						<th>LLAMADA</th>
						<th>AUDIO</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>
					</form>