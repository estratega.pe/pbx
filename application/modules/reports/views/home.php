          <div class="row">
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<a href="{BASE_URL}reports/general"><span class="info-box-icon bg-red"><i class="fa fa-user"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Reportes</span>
									<span class="info-box-number">Listado de Reportes</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<a href="{BASE_URL}reports/entrantes"><span class="info-box-icon bg-red"><i class="fa fa-user"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Entrantes</span>
									<span class="info-box-number">Reporte de llamadas</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<a href="{BASE_URL}reports/salientes"><span class="info-box-icon bg-red"><i class="fa fa-user"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Salientes</span>
									<span class="info-box-number">Reporte de llamadas</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<a href="{BASE_URL}reports/audios"><span class="info-box-icon bg-red"><i class="fa fa-user"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Audios</span>
									<span class="info-box-number">Descarga de Audios</span>
								</div>
							</div>
						</div>
					</div>