					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}pbx/user"><span class="info-box-icon bg-red"><i class="fa fa-user"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Usuarios</span>
									<span class="info-box-number">Administrar Usuarios</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}pbx/anexo"><span class="info-box-icon bg-red"><i class="fa fa-phone"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Anexos</span>
									<span class="info-box-number">Administrar Anexos</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}pbx/area"><span class="info-box-icon bg-red"><i class="fa fa-group"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Areas</span>
									<span class="info-box-number">Administrar Areas</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}pbx/contact"><span class="info-box-icon bg-red"><i class="fa fa-child"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Abonados</span>
									<span class="info-box-number">Administrar Abonados</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
					</div>