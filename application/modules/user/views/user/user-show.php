					<form method="post">
					<div class="row">
						<div class="col-xs-12">
              <div class="box">
								<div class="box-footer">
									<button type="submit" class="btn btn-primary pull-right">Guardar</button>
								</div>
							</div>
						</div>
					</div>
          <input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
          <div class="row">
						<div class="col-xs-6">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">No Molestar</h3>
                </div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="dnd">DND</label>
										{pbx_user_dnd}
									</div>
                </div>
							</div>
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">Detalles del usuario</h3>
                </div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_code">Nombres</label>
										{pbx_user_name}
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_code">Apellidos</label>
										{pbx_user_lastname}
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_nomolestar">E-mail</label>
										{email}
									</div>
								</div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_code">Grupo/Departamento</label>
										{AREA_SELECT}
									</div>
                </div>
								<div class="box box-warning"></div>
              </div><!-- /.box -->
						</div>
						<div class="col-xs-6">
              <div class="box">
								<div class="box-header">
									<h3 class="box-title">Desvio de Llamadas</h3>
                </div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="desvio">DESVIO</label>
										{pbx_user_desvio_select}
										<br />
										{pbx_user_desvio_input}
									</div>
                </div>
								<div class="box box-warning"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_name">Usuario</label>
										{pbx_user_username}
									</div>
                </div>
								<div class="box-body">
									<div class="form-group">
										<label for="int_name">Anexo</label>
										{ANEXO_SELECT}
									</div>
                </div>
								<div class="box-body box box-danger">
									<div class="form-group">
										<label for="pbx_user_callpassword">Clave de llamadas</label>
										{pbx_user_callpassword_text}
									</div>
                </div>
								<div class="box box-danger"></div>
								<div class="box-body">
									<div class="form-group">
										<label for="pbx_user_callpassword">Cambiar Clave</label>
										{pbx_user_callpassword}
									</div>
                </div>
								<div class="box box-warning"></div>
							</div>
						</div>
					</div>
					</form>