<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'user/';
	static $_rowsPage = 10;
	
	private $_typeDesvios = array(
		'0' => 'Ninguno'
		,'1'=> 'Anexos'
		,'2'=> 'Anexos y Locales'
		,'3'=> 'Anexos y Moviles'
		,'4'=> 'Locales y Moviles'
		,'5'=> 'Anexos, Locales y Moviles'
		,'6'=> 'Anexos, Locales, Moviles y Nacionales'
		,'7'=> 'Anexos, Locales, Moviles, Nacionales e Internacionales'
	);
	/**********************
	 * Codigos de salida  *
	 * 0 => Ninguno       *
	 * 1 => Anexo         *
	 * 2 => Local         *
	 * 3 => Movil         *
	 * 4 => Nacional      *
	 * 5 => Internacional *
	 *********************/
	private $_arrayTypesCf = array(
		'0'	=> array(
			'0'	=> 'Ninguno'
		),
		'1'	=> array(
			'0'	=> 'Ninguno'
			,'1'=> 'Anexos'
		),
		'2'	=> array(
			'0'	=> 'Ninguno'
			,'1'=> 'Anexo'
			,'2'=> 'Local'
		),
		'3'	=> array(
			'0'	=> 'Ninguno'
			,'1'=> 'Anexo'
			,'3'=> 'Movil'
		),
		'4'	=> array(
			'0'	=> 'Ninguno'
			,'2'=> 'Local'
			,'3'=> 'Movil'
		),
		'5'	=> array(
			'0'	=> 'Ninguno'
			,'1'=> 'Anexo'
			,'2'=> 'Local'
			,'3'=> 'Movil'
		),
		'6'	=> array(
			'0'	=> 'Ninguno'
			,'1'=> 'Anexo'
			,'2'=> 'Local'
			,'3'=> 'Movil'
			,'4'=> 'Nacional'
		),
		'7'	=> array(
			'0'	=> 'Ninguno'
			,'1'=> 'Anexo'
			,'2'=> 'Local'
			,'3'=> 'Movil'
			,'4'=> 'Nacional'
			,'5'=> 'Internacional'
		)
	);
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('userID') <= 0x0000) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function userShow
	 * Show a User
	 */
	public function index() {
		$userId = $this->session->userdata('userID');
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0;
		if( $userId <= 0x0000 ) {
			header('Location: ' . base_url() );
			exit();
		}
		#GET USER BY ID
		$u = $this->model->getDataRow('permisos', array('id' => $userId));
		if($u == '') {
			header('Location: ' . base_url() );
			exit();
		}
		$user = $this->model->getDataRow('user', array('user_id' => $u->id));
		
		/**
		 * BODY
		 */
		
		if($_POST) {
			if($userId != $u->id) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			$clave = $this->security->xss_clean($this->input->post('pbx_user_callpassword', TRUE));
			$dnd_activo = $this->security->xss_clean($this->input->post('pbx_user_nomolestar', TRUE));
			$desvio_opcion = $this->security->xss_clean($this->input->post('desvio_opcion', TRUE));
			$desvio_destino = $this->security->xss_clean($this->input->post('desvio_destino', TRUE));
			
			$dataUser = array();
			$dataUserPbx = array();
			
			$dataUserPbx['dnd_activo'] = $dnd_activo;
			$dataUserPbx['desvio_opcion'] = $desvio_opcion;
			$dataUserPbx['desvio_destino'] = $desvio_destino;
			
			if($clave == 0x0001) {
				$newPwd = generatePassword();
				$dataUser['password'] = $this->my_crypto->password($newPwd);
				$dataUserPbx['pwd_text'] = $newPwd;
			}
			
			if ( count($dataUser) >= 0x0001 || count($dataUserPbx) >= 0x0001) {
				if($clave == 0x0001) {
					$this->model->updateData('user', array('user_id' => $u->id), $dataUser);
				}
				$this->model->updateData('permisos', array('id' => $u->id), $dataUserPbx);
			}
			
			redirect(base_url() . $this->_moduleUrl, 'refresh');
			exit();
		}
		#Get and Set Areas
		$areas = $this->model->getData('area');
		$areaOption = array();
		$areaAdminOption = array();
		$areaAdminOption['X'] = 'Ninguno';
		foreach( $areas as $a) {
			$areaOption[$a->area_id] = $a->area_name;
			if($a->area_admin == 0)
				$areaAdminOption[$a->area_id] = $a->area_name;
		}
		$this->_data['AREA_SELECT'] = form_dropdown('pbx_user_areaselect', $areaOption, $u->grupo, 'class="form-control" disabled');
		#Get and Set Anexos
		$anexos = $this->model->getData('ps_endpoints');
		$anexoOption = array();
		$anexoOption['X'] = 'Ninguno';
		foreach($anexos as $an) {
			if(preg_match("/SIP_/", $an->id) || preg_match("/TRUNK_/", $an->id)) {
				continue;
			}
			$anexoOption[$an->id] = $an->id;
		}
		$this->_data['ANEXO_SELECT'] = form_dropdown('pbx_user_anexoselect', $anexoOption, $u->anexo, 'class="form-control" disabled');
		$this->_data['pbx_user_desvio_input'] = form_input('desvio_destino', $u->desvio_destino, ' class="form-control"');
		$this->_data['pbx_user_desvio_select'] = form_dropdown('desvio_opcion', $this->_arrayTypesCf[$u->desvio_tipo], $u->desvio_opcion, 'class="form-control" ');
		$this->_data['pbx_user_name'] = form_input('pbx_user_name', $user->first_name, ' class="form-control" readonly');
		$this->_data['pbx_user_lastname'] = form_input('pbx_user_lastname', $user->last_name, ' class="form-control" readonly');
		$this->_data['email'] = form_input('email', $user->email, ' class="form-control" readonly');
		$this->_data['pbx_user_dnd'] = ($u->dnd_can != 0x0000) ? form_checkbox('pbx_user_nomolestar', '1', ($u->dnd_activo == 0x0001) ? TRUE : FALSE, ' class="flat-red" ') : '';
		$this->_data['pbx_user_callpassword_text'] = form_input('pbx_user_callpassword_text', $u->pwd_text, ' class="form-control" readonly');
		$this->_data['pbx_user_callpassword'] = form_checkbox('pbx_user_callpassword', '1', FALSE, ' class="flat-red" ');
		
		$this->_data['pbx_user_username'] = form_input('pbx_user_username', $user->username, ' class="form-control" readonly');
		
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('user/user/user-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = $user->username;
		$this->_body['PAGE_DESCRIPTION'] = $user->first_name . ' ' . $user->last_name;
		addCSS($this->_baseUrl . 'assets/plugins/jQueryUI/custom-theme/jquery-ui-1.8.20.custom.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/jQueryUI/jquery-ui.min.js', 'bottom', 'file');
		generatePage($this->_body);
	} 
	/**
	 * function userDel
	 * Delete a User
	 */
	public function userDel() {
		$userId = $this->uri->segment(4);
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0;
		if($userId < 0x0002) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('pbx_user_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$userDel = $this->security->xss_clean($this->input->post('detele_id'));
			$userDel = filter_var($userDel,FILTER_VALIDATE_INT) ? $userDel : 0x0001;
			if($userId == 0x0001 || $userId != $userDel || $userDel == 0x0001) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('pbx_user_delete')) {
				$this->model->deleteData('permisos', array('id' => $userDel));
				$this->model->deleteData('user', array('user_id' => $userDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $userId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Usuario';
		generatePage($this->_body);
	}
	
}
