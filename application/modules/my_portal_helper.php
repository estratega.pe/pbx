<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *Function getMenu
 */
if ( ! function_exists('getMenu')) {
	function getMenu() {
		$retorno = array();
		$CI = & get_instance();
		$links = $CI->my_acl->getPermisos();
		if(!empty($links)) {
			foreach ($links as $k => $v) {
				$retorno[] = array(
					'MENU_LINK'		=> base_url() . $k
					,'MENU_TEXT'	=> strtoupper(str_replace('_', ' ',$k))
				);
			}
		}
		$menu = array();
		$menus = $CI->db->where('menu_parent', 0x0000)
			->get('menu')
			->result();
		$i = 0;
		$uri1 = $CI->uri->segment(1);
		$uri2 = $CI->uri->segment(2);
		
		if(empty($uri2)){
			$url2 = $uri1;
		} else {
			$url2 = $uri1 . '/' . $uri2;
		}		

		foreach($menus as $k) {
			$selected = '';
			
			$childrens = $CI->db->where('menu_parent', $k->menu_id)
				->order_by('menu_weight', 'ASC')
				->get('menu')
				->result();
			$_acl = $CI->my_acl->getPermisoKey($k->menu_perm);
			if($CI->my_acl->acceso($_acl)) {
				if(count($childrens) >= 0x0001) {
					$children = array();
					$menu[$i]['NORMAL'] = array();
					$selected_parent = false;
					foreach($childrens as $c) {
						$_acl = $CI->my_acl->getPermisoKey($c->menu_perm);
						if($CI->my_acl->acceso($_acl)) {
							$selected = '';
							if($c->menu_link == $url2) {
								$selected = 'active';
								$selected_parent = true;
							}
							$children[] = array(
								'MENU_LINK'		=> base_url() . $c->menu_link
								,'MENU_TEXT'	=> strtoupper($c->menu_name)
								,'MENU_ACTIVE' => $selected
							);
						}
					}
					$menu[$i]['MULTI'][] = array(
						'MENU_LINK'		=> $k->menu_link
						,'MENU_TEXT'	=> $k->menu_name
						,'MENU_CHILD'	=> $children
						,'MENU_ACTIVE' => ($selected_parent)? 'active' : ''
					);
				} else {
					$menu[$i]['NORMAL'][] = array(
						'MENU_LINK'		=> base_url() . $k->menu_link
						,'MENU_TEXT'	=> strtoupper($k->menu_name)
					);
					$menu[$i]['MULTI'] = array();
				}
				$i++;
			}
		}
		return $menu;
	}
}
/**
 *Function makeHeader
 */
if ( ! function_exists('makeHeader')) {
	function makeHeader() {
		$CI = & get_instance();
		$_u = $CI->session->userdata('userDATA');
		$css_top = $CI->session->userdata('css_top');
		$css_top = ($css_top =='') ? array() : $css_top;
		$css_inline = $CI->session->userdata('css_top_inline');
		$css_inline = ($css_inline =='') ? array() : $css_inline;
		
		$js_top = $CI->session->userdata('js_top');
		$js_top = ($js_top =='') ? array() : $js_top;
		$js_inline = $CI->session->userdata('js_top_inline');
		$js_inline = ($js_inline =='') ? array() : $js_inline;
		
		$header = array();
		$header['BASE_URL'] = base_url();
		$header['CSS'] = $css_top;
		$header['CSS_INLINE'] = $css_inline;
		$header['JS_INLINE'] = $js_inline;
		$header['JS'] = $js_top;
		$header['MENU'] = getMenu();
		$header['USERNAME'] = $_u['username'];
		$header['USER_ROLE'] = $CI->db->where('role_id', $_u['role'])->get('role')->row()->role_name;
		$CI->parser->parse('header', $header);
	}
}
/**
 *Function makeFooter
 */
if ( ! function_exists('makeFooter')) {
	function makeFooter() {
		$CI = & get_instance();
		$_u = $CI->session->userdata('userDATA');
		$css_bottom = $CI->session->userdata('css_bottom');
		$css_bottom = ($css_bottom =='') ? array() : $css_bottom;
		$js_bottom = $CI->session->userdata('js_bottom');
		$js_bottom = ($js_bottom =='') ? array() : $js_bottom;
		$js_inline = $CI->session->userdata('js_bottom_inline');
		$js_inline = ($js_inline =='') ? array() : $js_inline;
		$footer = array();
		$footer['BASE_URL'] = base_url();
		$footer['CSS'] = $css_bottom;
		$footer['JS'] = $js_bottom;
		$footer['JS_INLINE'] = $js_inline;
		$CI->parser->parse('footer', $footer);
	}
}
/**
 *Function getMemoryRam
 * return array(
 *  "MemTotal"		=> array("XXXX","kB")
 * ,"MemFree"			=> array("XXXX","kB")
 * ,"Buffers"			=> array("XXXX","kB")
 * ,"Cached"			=> array("XXXX","kB")
 * ,"SwapCached"	=> array("XXXX","kB")
 * ,"Active"			=> array("XXXX","kB")
 * );
 */
if ( ! function_exists('getMemoryRam')) {
	function getMemoryRam() {
		$data = explode("\n", file_get_contents("/proc/meminfo"));
		$meminfo = array();
		foreach ($data as $line) {
			list($key, $val) = explode(":", $line);
			$v = explode(trim($val));
			$meminfo[$key] = array(trim($v[0]),trim($v[1]));
		}
		return $meminfo;
	}
}
/**
 *Function generatePasswordText
 */
if ( ! function_exists('generatePasswordText')) {
	function generatePasswordText() {
		//Initialize the random password
		$password = '';
		//Initialize a random desired length
		$desired_length = rand(8, 12);
		for($length = 0; $length < $desired_length; $length++) {
			//Append a random ASCII character (including symbols)
			$password .= chr(rand(32, 126));
		}
		return $password;
	}
}
/**
 *Function addJS
 */
if ( ! function_exists('addJS')) {
	function addJS($_file, $pos = 'top', $type = 'file') {
		$CI = & get_instance();
		$_js = ($pos == 'top') ? 'js_top' : 'js_bottom';
		$_js = ($type == 'file') ? $_js : $_js . '_inline';
		$js = array();
		$_last = $CI->session->userdata($_js);
		$CI->session->unset_userdata($_js);
		if(is_array($_file)) {
			if($type == 'file') {
				
			} else {
				$content = $_last[0]['TEXT'];
				foreach($_file as $f) {
					$content .= $f . "\n";
				}
			}
			
		} else {
			if($type == 'file') {
				if(is_array($_last)) {
					foreach($_last as $k => $v ) {
						$js[]['FILE'] = $v['FILE'];
					}
				}
				$js[]['FILE'] = $_file;
			} else {
				$content = $_last[0]['TEXT'];
				$content .= $_file . "\n";
				$js[0]['TEXT'] = $content;
			}
		}
		$CI->session->set_tempdata($_js, $js, 2);
	}
}
/**
 *Function addCSS
 */
if ( ! function_exists('addCSS')) {
	function addCSS($_file, $pos = 'top', $type = 'file') {
		$CI = & get_instance();
		$_js = ($pos == 'top') ? 'css_top' : 'css_bottom';
		$_js = ($type == 'file') ? $_js : $_js . '_inline';
		$js = array();
		$_last = $CI->session->userdata($_js);
		$CI->session->unset_userdata($_js);
		if(is_array($_file)) {
			if($type == 'file') {
				
			} else {
				$content = $_last[0]['TEXT'];
				foreach($_file as $f) {
					$content .= $f . "\n";
				}
			}
			
		} else {
			if($type == 'file') {
				if(is_array($_last)) {
					foreach($_last as $k => $v ) {
						$js[]['FILE'] = $v['FILE'];
					}
				}
				$js[]['FILE'] = $_file;
			} else {
				$content = $_last[0]['TEXT'];
				$content .= $_file . "\n";
				$js[0]['TEXT'] = $content;
			}
		}
		$CI->session->set_tempdata($_js, $js, 2);
	}
}
/**
 *Function breadcrumbs
 */
if ( ! function_exists('breadcrumbs')) {
	function breadcrumbs() {
		$_domain = $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER['HTTP_HOST'] . '/';
		$_base = base_url();
		$_path = str_replace($_domain, '', $_base);
		$path = str_replace($_path . 'index.php/', '', $_SERVER["PHP_SELF"]);
		$parts = explode('/',$path);
		$link = '<li><a href="' . base_url() . '"><i class="fa fa-dashboard"></i> Inicio</a></li>';
		if (count($parts) > 2) {
			for ($i = 1; $i < count($parts); $i++) {
				$s = '';
				$c = '';
				if($i == 4)
					continue;
				for($j=1;$j<=$i;$j++) {
					$s .= $parts[$j] . '/';
				}
				if($i == count($parts) -1) {
					$link .= '<li class="active">' . ucfirst($parts[$i]) . '</li>';
				}  else {
					$link .= '<li><a href="' . base_url() . $s .'">' . ucfirst($parts[$i]) . '</a></li>';
				}
			}
		}
		return $link;
	}
}
if ( ! function_exists('generatePage')) {
	function generatePage($content) {
		$CI = & get_instance();
		makeHeader();
		$content['BREADCRUMBS'] = breadcrumbs();
		$CI->parser->parse('content', $content);
		makeFooter();
	}
}