					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}admintask/role"><span class="info-box-icon bg-red"><i class="fa fa-cogs"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Roles</span>
									<span class="info-box-number">Administrar Roles</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}admintask/permission"><span class="info-box-icon bg-red"><i class="fa fa-check"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Permisos</span>
									<span class="info-box-number">Administrar Permisos</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}admintask/menu"><span class="info-box-icon bg-red"><i class="fa fa-navicon"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Menus</span>
									<span class="info-box-number">Administrar Menus</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
						<div class="col-lg-3 col-xs-6">
							<div class="info-box">
								<!-- Apply any bg-* class to to the icon to color it -->
								<a href="{BASE_URL}admintask/user"><span class="info-box-icon bg-red"><i class="fa fa-users"></i></span></a>
								<div class="info-box-content">
									<span class="info-box-text">Usuarios</span>
									<span class="info-box-number">Administrar Usuarios</span>
								</div><!-- /.info-box-content -->
							</div><!-- /.info-box -->
						</div>
					</div>