					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div class="box box-warning"></div>
									<div class="box-header">
										<h3 class="box-title">Datos de Ingreso</h3>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Nombre de Usuario</label>
										<input type="text" class="form-control" placeholder="Ingrese Nombre de usuario" value="{USERNAME}" readonly>
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Puede hacer login </label>
											<i class="fa fa-{ACTIVE_CHECKED}"></i>
									</div>
									<div class="box box-warning"></div>
									<div class="box-header">
										<h3 class="box-title">Datos de Usuario</h3>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Nombres</label>
										<input type="text" class="form-control" placeholder="Ingrese Nombres" value="{FIRST_NAME}" readonly>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Apellidos</label>
										<input type="text" class="form-control" placeholder="Ingrese Apellidos" value="{LAST_NAME}" readonly>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">E-Mail</label>
										<input type="text" class="form-control" placeholder="Ingrese E-mail" value="{EMAIL}" readonly>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Rol</label>
										{ROLE_SELECT}
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Activo</label>
											<i class="fa fa-{STATUS_CHECKED}"></i>
									</div>
									<div class="box box-warning"></div>
									<div class="box-header">
										<h3 class="box-title">{TITLE_PERM}</h3>
									</div><!-- /.box-header -->
									{PERM_ITEM}
										<div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon">
													<i class="fa fa-{PERM_STATUS}"></i>
                        </span>
                        <input type="text" class="form-control" value="{PERM_TEXT}" readonly>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-12 -->
									{/PERM_ITEM}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>