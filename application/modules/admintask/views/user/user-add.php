					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <form method="post">
								<div class="box-body">
									<div class="box box-warning"></div>
									<div class="box-header">
										<h3 class="box-title">Datos de Ingreso</h3>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Nombre de Usuario</label>
										<input type="text" class="form-control" id="username" name="username" placeholder="Ingrese Nombre de usuario" required>
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Clave</label>
										<input type="text" class="form-control" id="password" name="password" placeholder="Ingrese Clave" required>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Puede hacer login </label>
											<input name="active" type="checkbox" class="flat-red" checked>
									</div>
									<div class="box box-warning"></div>
									<div class="box-header">
										<h3 class="box-title">Datos de Usuario</h3>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Nombres</label>
										<input type="text" class="form-control" id="first_name" name="first_name" placeholder="Ingrese Nombres" required>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Apellidos</label>
										<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Ingrese Apellidos" required>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">E-Mail</label>
										<input type="text" class="form-control" id="email" name="email" placeholder="Ingrese E-mail" required>
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Rol</label>
										{ROLE_SELECT}
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Activo</label>
											<input name="status" type="checkbox" class="flat-red" checked>
									</div>
									<div class="box box-warning"></div>
									<div class="box-header">
										<h3 class="box-title">{TITLE_PERM}</h3>
									</div><!-- /.box-header -->
									{PERM_ITEM}
										<div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input name="permission_id[{PERM_ID}]" type="checkbox" class="flat-red">
                        </span>
                        <input type="text" class="form-control" value="{PERM_TEXT}" readonly>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-12 -->
									{/PERM_ITEM}
                </div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
								</form>
              </div><!-- /.box -->
						</div>
					</div>