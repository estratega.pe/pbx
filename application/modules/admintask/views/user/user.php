					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
								{LINK_USER_ADD} 
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>USERNAME</th>
												<th>NOMBRES</th>
												<th>APELLIDOS</th>
												<th>EMAIL</th>
												<th>ROL</th>
												<th>LOGIN</th>
												<th>ESTADO</th>
                        <th>ACCIONES</th>
                      </tr>
                    </thead>
                    <tbody>
                      {USER_ITEM}
											<tr>
                        <td>{USERNAME}</td>
												<td>{FIRST_NAME}</td>
												<td>{LAST_NAME}</td>
												<td>{EMAIL}</td>
												<td>{ROLE}</td>
												<td><span class="label label-{ACTIVE_CLASS}">{ACTIVE_TEXT}</span></td>
												<td><span class="label label-{STATUS_CLASS}">{STATUS_TEXT}</span></td>
												<td>{ACTIONS}</td>
                      </tr>
											{/USER_ITEM}
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>USERNAME</th>
												<th>NOMBRES</th>
												<th>APELLIDOS</th>
												<th>EMAIL</th>
												<th>ROL</th>
												<th>LOGIN</th>
												<th>ESTADO</th>
                        <th>ACCIONES</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>
