					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
								<div class="box-body">
									<div class="form-group">
										<label for="exampleInputEmail1">Nombre del Rol</label>
										<input type="text" class="form-control" value="{ROLE_NAME}" readonly>
										<input type="hidden" name="role_id" value="{ROLE_ID}" />
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
									<div class="box-header">
										<h3 class="box-title">PERMISOS</h3>
									</div><!-- /.box-header -->
									{PERM_ITEM}
										<div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon">
													<i class="fa fa-{PERM_STATUS}"></i>
                        </span>
                        <input type="text" class="form-control" value="{PERM_TEXT}" readonly>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-12 -->
									{/PERM_ITEM}
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>