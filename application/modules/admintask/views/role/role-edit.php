					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <form method="post">
								<div class="box-body">
									<div class="form-group">
										<label for="exampleInputEmail1">Nombre del Rol</label>
										<input type="text" class="form-control" id="role_name" name="role_name" placeholder="Ingrese nombre nuevo del Rol" value="{ROLE_NAME}">
										<input type="hidden" name="role_id" value="{ROLE_ID}" />
										<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									</div>
									<div class="box-header">
										<h3 class="box-title">PERMISOS</h3>
									</div><!-- /.box-header -->
									
									<div class="col-lg-12">
										<div class="input-group">
											<span class="input-group-addon">
												<input id="select-all" type="checkbox" class="flat-red"> 
											</span>
											<input type="text" class="form-control" value="Seleccionar todos" readonly>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="input-group">
											&nbsp;
										</div>
									</div>
									
									{PERM_ITEM}
										<div class="col-lg-12">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <input name="permission_id[{PERM_ID}]" type="checkbox" class="flat-red select-ind" {PERM_STATUS}>
                        </span>
                        <input type="text" class="form-control" value="{PERM_TEXT}" readonly>
                      </div><!-- /input-group -->
                    </div><!-- /.col-lg-12 -->
									{/PERM_ITEM}
                </div><!-- /.box-body -->
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Guardar</button>
								</div>
								</form>
              </div><!-- /.box -->
						</div>
					</div>