					<!-- Small boxes (Stat box) -->
          <div class="row">
						<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
									<h3 class="box-title">{TITLE_BODY}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <form method="post">
									<input type="hidden" name="{TOKEN_NAME}" value="{TOKEN_VALUE}" />
									{CONFIGS}
									<div class="form-group">
										<label for="exampleInputEmail1">{cdp_label}</label>
										<input type="text" class="form-control" id="{cdp_key}" name="{cdp_key}" value="{cdp_value}" required>
									</div>
									{/CONFIGS}
									<div class="box-footer">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>
									</form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
						</div>
					</div>