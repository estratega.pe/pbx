<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admintaskmodel extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	/************
	 * Get DATA *
	 ***********/
	public function getData($table, $order = 'ASC', $campo = 'X')
	{
		if($campo!='X')
		{
			$this->db->order_by($campo, $order); 
		}
		$query = $this->db->get($table);
		return $query->result();
	}
	/******************
	 * Get DATA Where *
	 *****************/
	public function getDataWhere($table, $where, $order ='ASC', $campo='X', $limitS = 'X', $limitE = 'X')
	{
		$this->db->where($where);
		$this->db->from($table);
		if($campo!='X')
		{
			$this->db->order_by($campo, $order); 
		}
		if($limitS != 'X' && $limitE !='X')
		{
			$this->db->limit($limitE, $limitS);
		}
		$query = $this->db->get();
		return $query->result();
	}
	/****************
	 * Get DATA Row *
	 ***************/
	public function getDataRow($table, $where)
	{
		$this->db->where($where);
		$this->db->from($table);
		$query = $this->db->get();
		return $query->row();
	}
	/***************
	 * Create DATA *
	 **************/
	public function createData($table, $data)
	{
		$this->db->insert($table, $data);
	}
	/***************
	 * Update DATA *
	 **************/
	public function updateData($table, $where, $data)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	/***************
	 * Delete DATA *
	 **************/
	public function deleteData($table, $where)
	{
		$this->db->delete($table, $where);
	}
}
