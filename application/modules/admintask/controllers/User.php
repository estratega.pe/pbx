<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'admintask/user';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('user_list')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function user
	 * List all users
	 */
	public function index() {
		if(!$this->my_acl->acceso('user_list')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET USERS
		$users = $this->model->getData('user');
		$user = array();
		foreach($users  as $k) {
			$show = (!$this->my_acl->acceso('user_show')) ? '' : anchor(base_url() . 'admintask/user/userShow/' . $k->user_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' ';
			$edit = (!$this->my_acl->acceso('user_edit')) ? '' : anchor(base_url() . 'admintask/user/userEdit/' . $k->user_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$del = (!$this->my_acl->acceso('user_delete')) ? '' : anchor(base_url() . 'admintask/user/userDel/' . $k->user_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"') . ' ';
			$del = ($k->user_id == 0x0001) ? '' : $del;
			array_push($user, array(
				'USERNAME'			=> $k->username
				,'FIRST_NAME'		=> $k->first_name
				,'LAST_NAME'		=> $k->last_name
				,'EMAIL'				=> $k->email
				,'ROLE'				=> $this->model->getDataRow('role', array('role_id' => $k->role))->role_name
				,'STATUS_CLASS'	=> ($k->status == '1') ? 'success' : 'danger'
				,'STATUS_TEXT'	=> ($k->status == '1') ? 'ACTIVO' : 'INACTIVO'
				,'ACTIVE_CLASS'	=> ($k->active == '1') ? 'success' : 'danger'
				,'ACTIVE_TEXT'	=> ($k->active == '1') ? 'SI' : 'NO'
				,'ACTIONS'			=> $show
				. $edit
				. $del
			));
		}
		$this->_data['USER_ITEM'] = $user;
		$this->_data['LINK_USER_ADD'] = (!$this->my_acl->acceso('user_add')) ? '' : anchor(base_url() . 'admintask/user/userAdd', '<i class="fa fa-plus"></i>', 'title="Agregar"') . ' ';
		$this->_data['TITLE_BODY'] = "Usuario";
		$body = $this->parser->parse('admintask/user/user', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de usuarios';
		addCSS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/jquery.dataTables.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.min.js', 'bottom', 'file');
		addJS('$("#example2").DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false
		});', 'bottom', 'inline');
		generatePage($this->_body);
	}
	
	/**
	 * function userAdd
	 * Add new User
	 */
	public function userAdd() {
		if(!$this->my_acl->acceso('user_add')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		if($_POST) {
			$username = $this->security->xss_clean($this->input->post('username', TRUE));
			$clave = $this->security->xss_clean($this->input->post('password', TRUE));
			$first_name = $this->security->xss_clean($this->input->post('first_name', TRUE));
			$last_name = $this->security->xss_clean($this->input->post('last_name', TRUE));
			$email = $this->security->xss_clean($this->input->post('email', TRUE));
			$role = $this->security->xss_clean($this->input->post('role'));
			$active = $this->security->xss_clean($this->input->post('active'));
			$status = $this->security->xss_clean($this->input->post('status'));
			$this->model->createData('user', array(
				'username'		=> $username
				,'first_name'	=> $first_name
				,'last_name'	=> $last_name
				,'email'			=> $email
				,'role'				=> $role
				,'status'			=> (empty($status)) ? '0' : '1'
				,'active'			=> (empty($active)) ? '0' : '1'
				,'password'		=> $this->my_crypto->password($clave)
				,'created'		=> $this->session->userdata('userID')
				,'created_at'	=> date('Y-m-d H:i:s')
			));
			$newUser = $this->db->insert_id();
			foreach($_POST['permission_id'] as $k => $v) {
				$this->model->createData('user_permission', array(
					'user_id'		=> $newUser
					,'perm'		=> $k
					,'value'	=> 1
				));
			}
			redirect(base_url() . 'admintask/user/userShow/' . $newUser, 'refresh');
			exit();
		}
		#GET PERMISSIONS
		$perm = array();
		if ($this->my_acl->acceso('admin_task')) {
			$perms = $this->model->getData('permission');
			foreach($perms  as $k) {
				array_push($perm, array(
					'PERM_ID'			=> $k->permission_id
					,'PERM_TEXT'	=> $k->perm
				));
			}
		}
		$roles = $this->model->getData('role');
		$role_options = array();
		foreach($roles as $k) {
			$role_options[$k->role_id] = $k->role_name;
		}
		$this->_data['ROLE_SELECT'] = form_dropdown('role', $role_options, 'X', 'class="form-control"');
		$this->_data['PERM_ITEM'] = $perm;
		$this->_data['TITLE_PERM'] = (!$this->my_acl->acceso('admin_task')) ? '' : 'PERMISOS';
		$this->_data['TITLE_BODY'] = "Usuario";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('admintask/user/user-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Usuario';
		generatePage($this->_body);
	}
	/**
	 * function userShow
	 * Show a User
	 */
	public function userShow() {
		$userId = $this->uri->segment(4);
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0;
		if($userId < 0x0001) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('user_show')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		#GET USER BY ID
		$user = $this->model->getDataRow('user', array('user_id' => $userId));
		#GET ROLES
		$perm = array();
		if ($this->my_acl->acceso('admin_task')) {
			$perms = $this->model->getData('permission');
			foreach($perms  as $k) {
				array_push($perm, array(
					'PERM_ID'			=> $k->permission_id
					,'PERM_TEXT'	=> $k->perm
				));
			}
		}
		$roles = $this->model->getData('role');
		$role_options = array();
		foreach($roles as $k) {
			$role_options[$k->role_id] = $k->role_name;
		}
		#GET PERMISSIONS BY USER ID
		$_perms = $this->model->getDataWhere('user_permission', array('user_id' => $userId));
		$_perm = array();
		foreach($_perms as $k) {
			$_perm[$k->perm] = $k->perm;
		}
		#GET PERMISSIONS
		$perm = array();
		if ($this->my_acl->acceso('admin_task')) {
			$perms = $this->model->getData('permission');
			foreach($perms  as $k) {
				$checked = (in_array($k->permission_id, $_perm)) ? 'check-square-o' : 'square-o';
				array_push($perm, array(
					'PERM_ID'				=> $k->permission_id
					,'PERM_TEXT'		=> $k->perm
					,'PERM_STATUS'	=> $checked
				));
			}
		}
		$this->_data['ACTIVE_CHECKED'] = ($user->active == '1') ? 'check-square-o' : 'square-o';
		$this->_data['STATUS_CHECKED'] = ($user->status == '1') ? 'check-square-o' : 'square-o';
		$this->_data['USERNAME'] = $user->username;
		$this->_data['FIRST_NAME'] = $user->first_name;
		$this->_data['LAST_NAME'] = $user->last_name;
		$this->_data['EMAIL'] = $user->email;
		$this->_data['ROLE_SELECT'] = form_dropdown('role', $role_options, $user->role, 'class="form-control" disabled');
		$this->_data['PERM_ITEM'] = $perm;
		$this->_data['TITLE_PERM'] = (!$this->my_acl->acceso('admin_task')) ? '' : 'PERMISOS';
		$this->_data['TITLE_BODY'] = "Usuario";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('admintask/user/user-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Usuario ' . $user->username;
		generatePage($this->_body);
	}
	/**
	 * function userEdit
	 * Edit a User
	 */
	public function userEdit() {
		$userId = $this->uri->segment(4);
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0x0000;
		if($userId < 0x0001) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('user_edit')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET USER BY ID
		$user = $this->model->getDataRow('user', array('user_id' => $userId));
		/**
		 * BODY
		 */
		if($_POST) {
			if($userId != $user->user_id) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			$clave = $this->security->xss_clean($this->input->post('password', TRUE));
			$first_name = $this->security->xss_clean($this->input->post('first_name', TRUE));
			$last_name = $this->security->xss_clean($this->input->post('last_name', TRUE));
			$email = $this->security->xss_clean($this->input->post('email', TRUE));
			$role = $this->security->xss_clean($this->input->post('role'));
			$active = $this->security->xss_clean($this->input->post('active'));
			$active = (empty($active)) ? '0' : '1';
			$active = ($user->user_id == 0x0001) ? '1' : $active;
			$status = $this->security->xss_clean($this->input->post('status'));
			$status = (empty($status)) ? '0' : '1';
			$status = ($user->user_id == 0x0001) ? '1' : $status;
			$data = array(
				'first_name'	=> $first_name
				,'last_name'	=> $last_name
				,'email'			=> $email
				,'role'				=> ($user->user_id == 0x0001) ? 0x0001 : $role
				,'status'			=> $status
				,'active'			=> $active
				,'modified'		=> $this->session->userdata('userID')
				,'modified_at'	=> date('Y-m-d H:i:s')
			);
			if(!empty($clave)) {
				$data['password'] = $this->my_crypto->password($clave);
			}
			$this->model->updateData('user', array('user_id' => $userId), $data);
			if ($this->my_acl->acceso('admin_task')) {
				$this->model->deleteData('user_permission', array('user_id' => $userId));
				if($user->user_id == 0x0001 || $userId == 0x0001) {
					//$this->model->deleteData('user_permission', array('user_id' => 0x0001));
					$_perms = $this->model->getData('permission');
					foreach($_perms  as $k) {
						$this->model->createData('user_permission', array(
							'user_id'	=> 0x0001
							,'perm'		=> $k->permission_id
							,'value'	=> 1
						));
					}
				} else {
					if(isset($_POST['permission_id'])) {
						foreach($_POST['permission_id'] as $k => $v) {
							$this->model->createData('user_permission', array(
								'user_id'	=> $userId
								,'perm'		=> $k
								,'value'	=> 1
							));
						}
					}
				}
			}
			redirect(base_url() . 'admintask/user/userShow/' . $userId, 'refresh');
			exit();
		}
		#GET ROLES
		$perm = array();
		if ($this->my_acl->acceso('admin_task')) {
			$perms = $this->model->getData('permission');
			foreach($perms  as $k) {
				array_push($perm, array(
					'PERM_ID'			=> $k->permission_id
					,'PERM_TEXT'	=> $k->perm
				));
			}
		}
		$roles = $this->model->getData('role');
		$role_options = array();
		foreach($roles as $k) {
			$role_options[$k->role_id] = $k->role_name;
		}
		#GET PERMISSIONS BY USER ID
		$_perms = $this->model->getDataWhere('user_permission', array('user_id' => $userId));
		$_perm = array();
		foreach($_perms as $k) {
			$_perm[$k->perm] = $k->perm;
		}
		#GET PERMISSIONS
		$perm = array();
		if ($this->my_acl->acceso('admin_task')) {
			$perms = $this->model->getData('permission');
			foreach($perms  as $k) {
				$checked = (in_array($k->permission_id, $_perm)) ? 'checked' : '';
				array_push($perm, array(
					'PERM_ID'				=> $k->permission_id
					,'PERM_TEXT'		=> $k->perm
					,'PERM_STATUS'	=> $checked
				));
			}
		}
		$this->_data['ACTIVE_CHECKED'] = ($user->active == '1') ? 'checked' : '';
		$this->_data['STATUS_CHECKED'] = ($user->status == '1') ? 'checked' : '';
		$this->_data['USERNAME'] = $user->username;
		$this->_data['FIRST_NAME'] = $user->first_name;
		$this->_data['LAST_NAME'] = $user->last_name;
		$this->_data['EMAIL'] = $user->email;
		$this->_data['ROLE_SELECT'] = form_dropdown('role', $role_options, $user->role, 'class="form-control"');
		$this->_data['PERM_ITEM'] = $perm;
		$this->_data['TITLE_PERM'] = (!$this->my_acl->acceso('admin_task')) ? '' : 'PERMISOS';
		$this->_data['TITLE_BODY'] = "Usuario";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('admintask/user/user-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Usuario';
		generatePage($this->_body);
	} 
	/**
	 * function userDel
	 * Delete a User
	 */
	public function userDel() {
		$userId = $this->uri->segment(4);
		$userId = filter_var($userId,FILTER_VALIDATE_INT) ? $userId : 0;
		if($userId < 0x0002) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('user_delete')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$userDel = $this->security->xss_clean($this->input->post('detele_id'));
			$userDel = filter_var($userDel,FILTER_VALIDATE_INT) ? $userDel : 0x0001;
			if($userId == 0x0001 || $userId != $userDel || $userDel == 0x0001) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('user_delete')) {
				$this->model->deleteData('user', array('user_id' => $userDel));
				$this->model->deleteData('permisos', array('id' => $userDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $userId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Usuarios';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Usuario';
		generatePage($this->_body);
	}
	
}
