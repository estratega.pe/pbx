<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'admintask/role';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('admin_task')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	/**
	 * function role
	 * List all roles
	 */
	public function index() {
		/**
		 * BODY
		 */
		#GET ROLES
		$roles = $this->model->getData('role');
		$rol = array();
		foreach($roles  as $k) {
			$edit = ($k->role_id == 0x0001) ? '' : anchor(base_url() . 'admintask/role/roleEdit/' . $k->role_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = ($k->role_id == 0x0001) ? '' : anchor(base_url() . 'admintask/role/roleDel/' . $k->role_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($rol, array(
				'ROLE'			=> $k->role_name
				,'ACTIONS'	=> anchor(base_url() . 'admintask/role/roleShow/' . $k->role_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' '
				. $edit
				. $delete
			));
		}
		$this->_data['ROLE_ITEM'] = $rol;
		$this->_data['TITLE_BODY'] = "Rol";
		$body = $this->parser->parse('admintask/role/role', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Roles';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de roles';
		addCSS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/jquery.dataTables.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.min.js', 'bottom', 'file');
		addJS('$("#example2").DataTable();', 'bottom', 'inline');
		generatePage($this->_body);
	}
	
	/**
	 * function roleAdd
	 * Add new Role
	 */
	public function roleAdd() {
		/**
		 * BODY
		 */
		if($_POST) {
			$role_name = $this->security->xss_clean($this->input->post('role_name', TRUE));
			$this->model->createData('role', array('role_name' => $role_name));
			$newRole = $this->db->insert_id();
			foreach($_POST['permission_id'] as $k => $v) {
				$this->model->createData('role_permission', array(
					'role'		=> $newRole
					,'perm'		=> $k
					,'value'	=> 1
				));
			}
			redirect(base_url() . 'admintask/role', 'refresh');
			exit();
		}
		#GET ROLES
		$perms = $this->model->getData('permission');
		$perm = array();
		foreach($perms  as $k) {
			array_push($perm, array(
				'PERM_ID'			=> $k->permission_id
				,'PERM_TEXT'	=> $k->perm
			));
		}
		$this->_data['PERM_ITEM'] = $perm;
		$this->_data['TITLE_BODY'] = "Rol";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('admintask/role/role-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Roles';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Rol';
		
		addJS("
			$('#select-all').change(function() {
				var checkboxes = $('.select-ind');
				if($(this).is(':checked')) {
						checkboxes.prop('checked', true);
				} else {
						checkboxes.prop('checked', false);
				}
			});
		", 'bottom', 'inline');
		
		generatePage($this->_body);
	}
	/**
	 * function roleShow
	 * Show a Role
	 */
	public function roleShow() {
		$roleId = $this->uri->segment(4);
		$roleId = filter_var($roleId,FILTER_VALIDATE_INT) ? $roleId : 0;
		#GET ROLE BY ID
		$role = $this->model->getDataRow('role', array('role_id' => $roleId));
		if($roleId == 0x0001 || $role->role_id == 0x0001) {
			$this->model->deleteData('role_permission', array('role' => 0x0001));
			$_perms = $this->model->getData('permission');
			foreach($_perms  as $k) {
				$this->model->createData('role_permission', array(
					'role'	=> 0x0001
					,'perm'		=> $k->permission_id
					,'value'	=> 1
				));
			}
		}
		/**
		 * BODY
		 */
		#GET PERMISSIONS BY ROLE ID
		$_perms = $this->model->getDataWhere('role_permission', array('role' => $roleId));
		$_perm = array();
		foreach($_perms as $k) {
			$_perm[$k->perm] = $k->perm;
		}
		#GET PERMISSIONS
		$perm = array();
		if ($this->my_acl->acceso('admin_task')) {
			$perms = $this->model->getData('permission');
			foreach($perms  as $k) {
				$checked = (in_array($k->permission_id, $_perm)) ? 'check-square-o' : 'square-o';
				array_push($perm, array(
					'PERM_ID'				=> $k->permission_id
					,'PERM_TEXT'		=> $k->perm
					,'PERM_STATUS'	=> $checked
				));
			}
		}
		$this->_data['TITLE_BODY'] = "Rol";
		$this->_data['ROLE_NAME'] = $role->role_name;
		$this->_data['PERM_ITEM'] = $perm;
		$body = $this->parser->parse('admintask/role/role-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Roles';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Rol ' . $role->role_name;
		generatePage($this->_body);
	}
	/**
	 * function roleEdit
	 * Edit a Role
	 */
	public function roleEdit() {
		$roleId = $this->uri->segment(4);
		$roleId = filter_var($roleId,FILTER_VALIDATE_INT) ? $roleId : 0;
		#GET ROLE BY ID
		$role = $this->model->getDataRow('role', array('role_id' => $roleId));
		if($roleId < 0x0001 || $role->role_id == 0x0001) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		if($_POST) {
			if($roleId != $role->role_id || $roleId < 0x0001 || $role->role_id == 0x0001) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			$role_name = $this->security->xss_clean($this->input->post('role_name', TRUE));
			$roleId = $this->security->xss_clean($this->input->post('role_id', TRUE));
			$this->model->updateData('role', array('role_id' => $roleId), array('role_name' => $role_name));
			$this->model->deleteData('role_permission', array('role' => $roleId));
			foreach($_POST['permission_id'] as $k => $v) {
				$this->model->createData('role_permission', array(
					'role'		=> $roleId
					,'perm'		=> $k
					,'value'	=> 1
				));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		#GET PERMISSIONS BY ROLE ID
		$_perms = $this->model->getDataWhere('role_permission', array('role' => $role->role_id));
		$_perm = array();
		foreach($_perms as $k) {
			$_perm[$k->perm] = $k->perm;
		}
		#GET PERMISSIONS
		$perms = $this->model->getData('permission');
		$perm = array();
		foreach($perms  as $k) {
			$checked = (in_array($k->permission_id, $_perm)) ? 'checked' : '';
			array_push($perm, array(
				'PERM_ID'				=> $k->permission_id
				,'PERM_TEXT'		=> $k->perm
				,'PERM_STATUS'	=> $checked
			));
		}
		$this->_data['PERM_ITEM'] = $perm;
		$this->_data['TITLE_BODY'] = "Rol";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['ROLE_NAME'] = $role->role_name;
		$this->_data['ROLE_ID'] = $role->role_id;
		$body = $this->parser->parse('admintask/role/role-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Roles';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Rol';
		
		addJS("
			$('#select-all').change(function() {
				var checkboxes = $('.select-ind');
				if($(this).is(':checked')) {
						checkboxes.prop('checked', true);
				} else {
						checkboxes.prop('checked', false);
				}
			});
		", 'bottom', 'inline');
		
		generatePage($this->_body);
	} 
	/**
	 * function roleDel
	 * Delete a Role
	 */
	public function roleDel() {
		$roleId = $this->uri->segment(4);
		$roleId = filter_var($roleId,FILTER_VALIDATE_INT) ? $roleId : 0;
		if($roleId < 0x0002) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('admin_task')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$roleDel = $this->security->xss_clean($this->input->post('detele_id'));
			$roleDel = filter_var($roleDel,FILTER_VALIDATE_INT) ? $roleDel : 0x0001;
			if($roleId == 0x0001 || $roleId != $roleDel || $roleDel == 0x0001) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('admin_task')) {
				$this->model->deleteData('role', array('role_id' => $roleDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $roleId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Roles';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Rol';
		generatePage($this->_body);
	}
	
}