<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'admintask/permission';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('admin_task')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	
	/**
	 * function permission
	 * List all Permission
	 */
	public function index() {
		/**
		 * BODY
		 */
		#GET PERMISSION
		$permission = $this->model->getData('permission');
		$perm = array();
		foreach($permission  as $k) {
			$edit = ($k->permission_id == 0x0001) ? '' : anchor(base_url() . 'admintask/permission/permissionEdit/' . $k->permission_id, '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>', 'title="Editar"') . ' ';
			$delete = ($k->permission_id == 0x0001) ? '' : anchor(base_url() . 'admintask/permission/permissionDel/' . $k->permission_id, '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>', 'title="Eliminar"');
			array_push($perm, array(
				'PERMISSION'	=> $k->perm
				,'KEY'				=> $k->key
				,'ACTIONS'		=> anchor(base_url() . 'admintask/permission/permissionShow/' . $k->permission_id, '<button class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>', 'title="Ver"') . ' '
				. $edit
				. $delete
			));
		}
		$this->_data['PERMISSION_ITEM'] = $perm;
		$this->_data['TITLE_BODY'] = "Permiso";
		$body = $this->parser->parse('admintask/permission/permission', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Permisos';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de permisos';
		addCSS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.css', 'top', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/jquery.dataTables.min.js', 'bottom', 'file');
		addJS($this->_baseUrl . 'assets/plugins/datatables/dataTables.bootstrap.min.js', 'bottom', 'file');
		addJS('$("#example2").DataTable();', 'bottom', 'inline');
		generatePage($this->_body);
	}
	/**
	 * function permissionAdd
	 * Add new Permission
	 */
	public function permissionAdd() {
		/**
		 * BODY
		 */
		if($_POST) {
			$perm = $this->security->xss_clean($this->input->post('perm', TRUE));
			$key = $this->security->xss_clean($this->input->post('key', TRUE));
			$this->model->createData('permission', array(
				'perm'	=> $perm
				,'key'	=> $key
			));
			redirect(base_url() . 'admintask/permission', 'refresh');
			exit();
		}
		$this->_data['TITLE_BODY'] = "Permiso";
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('admintask/permission/permission-add', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Permisos';
		$this->_body['PAGE_DESCRIPTION'] = 'Agregar nuevo Permiso';
		generatePage($this->_body);
	}
	/**
	 * function permissionShow
	 * Show a Permission
	 */
	public function permissionShow() {
		$permissionId = $this->uri->segment(4);
		$permissionId = filter_var($permissionId,FILTER_VALIDATE_INT) ? $permissionId : 0;
		#GET PERMISSION BY ID
		$permission = $this->model->getDataRow('permission', array('permission_id' => $permissionId));
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Permiso";
		$this->_data['PERM_NAME'] = $permission->perm;
		$this->_data['PERM_KEY'] = $permission->key;
		$body = $this->parser->parse('admintask/permission/permission-show', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Permisos';
		$this->_body['PAGE_DESCRIPTION'] = 'Ver Permiso ' . $permission->perm;
		generatePage($this->_body);
	}
	/**
	 * function permissionEdit
	 * Edit a Permission
	 */
	public function permissionEdit() {
		$permissionId = $this->uri->segment(4);
		$permissionId = filter_var($permissionId,FILTER_VALIDATE_INT) ? $permissionId : 0;
		#GET PERMISSION BY ID
		$permission = $this->model->getDataRow('permission', array('permission_id' => $permissionId));
		if($permissionId < 0x0001 || $permission->permission_id == 0x0001) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			if($permissionId != $permission->permission_id || $permissionId < 0x0001 || $permission->permissioin_id == 0x0001) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			$perm = $this->security->xss_clean($this->input->post('perm', TRUE));
			$this->model->updateData('permission', array('permission_id' => $permissionId), array('perm' => $perm));
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['TITLE_BODY'] = "Permiso";
		$this->_data['PERM_NAME'] = $permission->perm;
		$this->_data['PERM_KEY'] = $permission->key;
		$this->_data['PERM_ID'] = $permission->permission_id;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('admintask/permission/permission-edit', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Permisos';
		$this->_body['PAGE_DESCRIPTION'] = 'Editar Permiso ' . $permission->perm;
		generatePage($this->_body);
	} 
	/**
	 * function permissionDel
	 * Delete a Permission
	 */
	public function permissionDel() {
		$permissionId = $this->uri->segment(4);
		$permissionId = filter_var($permissionId,FILTER_VALIDATE_INT) ? $permissionId : 0;
		if($permissionId < 0x0002) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if(!$this->my_acl->acceso('admin_task')) {
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		if($_POST) {
			$permissionDel = $this->security->xss_clean($this->input->post('detele_id'));
			$permissionDel = filter_var($permissionDel,FILTER_VALIDATE_INT) ? $permissionDel : 0x0001;
			if($permissionId == 0x0001 || $permissionId != $permissionDel || $permissionDel == 0x0001) {
				header('Location: ' . base_url() . $this->_moduleUrl);
				exit();
			}
			if ($this->my_acl->acceso('admin_task')) {
				$this->model->deleteData('permission', array('permission_id' => $permissionDel));
			}
			header('Location: ' . base_url() . $this->_moduleUrl);
			exit();
		}
		/**
		 * BODY
		 */
		$this->_data['DELETE_VALUE'] = $permissionId;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$this->_data['TITLE_BODY'] = "Eliminar";
		$body = $this->parser->parse('delete', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Permisos';
		$this->_body['PAGE_DESCRIPTION'] = 'Eliminar Permiso';
		generatePage($this->_body);
	}
	
}