<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'admintask/permission';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('admin_task')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	public function index() {
		/**
		 * BODY
		 */
		if($_POST) {
			foreach($_POST as $k => $v) {
				$this->model->updateData('cdp_core_config', array('cdp_key' => $this->security->xss_clean($k)), array('cdp_value' => $this->security->xss_clean($v)));
			}
		}
		$configs = $this->model->getData('cdp_core_config');
		$this->_data['TITLE_BODY'] = "Configuracion de la plataforma";
		$this->_data['CONFIGS'] = $configs;
		$this->_data['TOKEN_NAME'] = $this->security->get_csrf_token_name();
		$this->_data['TOKEN_VALUE'] = $this->security->get_csrf_hash();
		$body = $this->parser->parse('admintask/config/config', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Configuraciones';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuraciones de la plataforma';
		generatePage($this->_body);
	}
}