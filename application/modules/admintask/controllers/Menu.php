<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MX_Controller {

	private $_baseUrl;
	private $_userDATA;
	private $_username;
	private $_role;
	private $_group;
	private $_body;
	private $_data;
	private $_footer;
	private $_moduleUrl = 'admintask/menu';
	static $_rowsPage = 10;
	
	function __construct()
	{
		parent::__construct();
		if(!$this->my_acl->acceso('admin_task')) {
			header('Location: ' . base_url());
			exit();
		}
		$this->_userDATA = $this->session->userdata('userDATA');
		$this->_username = $this->_userDATA['username'];
		$this->_role = $this->_userDATA['role'];
		$this->_group = $this->_userDATA['group'];
		$this->_baseUrl = base_url();
		$this->_data = array();
		$this->_data['BASE_URL'] = $this->_baseUrl;
		$this->_body = array();
		$this->_body['BASE_URL'] = $this->_baseUrl;
		$this->_footer = array();
		$this->_footer['BASE_URL'] = $this->_baseUrl;
		$this->load->model('admintask/Admintaskmodel', 'model');
	}
	
	
	/**
	 * function menu
	 * List all Menu
	 */
	public function index() {
		/**
		 * BODY
		 */
		#GET MENU's
		$menus = $this->model->getDataWhere('menu', array('menu_parent' => 0x0000), 'ASC', 'menu_weight');
		$menu = array();
		foreach($menus as $k) {
			$children = array();
			$childrens = $this->model->getDataWhere('menu', array('menu_parent' => $k->menu_id), 'ASC', 'menu_weight');
			foreach($childrens as $c) {
				$children[] = array(
						'ITEM_NAME'	=> $c->menu_name
						,'ITEM_ID'	=> $c->menu_id
					);
			}
			array_push($menu, array(
				'ITEM_NAME'				=> $k->menu_name
				,'ITEM_ID'				=> $k->menu_id
				,'MENU_CHILDREN'	=> $children
			));
		}
		$this->_data['MENU_ITEM'] = $menu;
		$this->_data['TITLE_BODY'] = "Menu";
		$body = $this->parser->parse('admintask/menu/menu', $this->_data, TRUE);
		$this->_body['CONTENT_PAGE'] = $body;
		$this->_body['PAGE_TITLE'] = 'Menu\'s';
		$this->_body['PAGE_DESCRIPTION'] = 'Configuracion de Menu\'s';
		addJS($this->_baseUrl . 'assets/plugins/Nestable/jquery.nestable.js', 'bottom', 'file');
		addJS('$("#menu_items").nestable({
			maxDepth: 2
		});', 'bottom', 'inline');
		generatePage($this->_body);
	}
}