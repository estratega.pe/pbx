<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	 *Function pbxConfig
	 */
	if ( ! function_exists('pbxConfig')) {
		function pbxConfig() {
			return array(
				'host'	=> '127.0.0.1'
				,'port'	=> 5038
				,'user'	=> 'd1erasipbx'
				,'pwd'	=> '0ec41aff77cfa5d3309123873b3615ce473825c736e27b4ddba9735dc1a5e6ac'
				,'timeout'	=> 128
			);
		}
	}
	/**
	 *Function pbxSendCommand
	 */
	if ( ! function_exists('pbxSendCommand')) {
		function pbxSendCommand($command) {
			$cfg = pbxConfig();
			include_once dirname(__FILE__) . '/AsteriskManager.php';
			include_once dirname(__FILE__) . '/AsteriskManagerException.php';
			$ast = new Net_AsteriskManager(array('server' => $cfg['host'], 'port' => $cfg['port']));
			try {
				$ast->connect();
			} catch (PEAR_Exception $e) {
				echo $e;
			}
			try {
				$ast->login($cfg['user'], $cfg['pwd']);
			} catch(PEAR_Exception $e) {
				echo $e;
			}
			$ast->command($command);
			
			/*
			//$socket = fsockopen($cfg['host'], $cfg['port'], $errno, $errstr, $cfg['timeout']);
			
			$socket = fsockopen($cfg['host'],$cfg['port'],$errno,$cfg['timeout']);
			fputs($socket, "Action: Login\r\n");
			fputs($socket, "UserName: " . $cfg['user'] . "\r\n");
			fputs($socket, "Secret: " . $cfg['pwd'] . "\r\n\r\n");

			fputs($socket, "Action: Command\r\n");
			fputs($socket, "Command: ".$command."\r\n\r\n");
			$wrets=fgets($socket,128);
			*/
		}
	}
	/**
	 *Function pbxRegenerateProviders
	 */
	if ( ! function_exists('pbxRegeneratePJSip')) {
		function pbxRegeneratePJSip() {
			$string = ";--\n@Author\t\t Jose Luis Huamani Gonzales.\n@Dev\t\t Jose Luis Huamani Gonzales\n@Personal\t JoseLuisGonzales@mail.ru\n--;\n\n";
			$vmlegend = ";--\n@Author\t\t Jose Luis Huamani Gonzales.\n@Dev\t\t Jose Luis Huamani Gonzales\n@Personal\t JoseLuisGonzales@mail.ru\n--;\n\n";
			$vmcontext = "";
			$CI = & get_instance();
			$_u = $CI->session->userdata('userDATA');
			$data = $CI->db->get('ps_registrations');
			foreach($data->result() as $row) {
				$_auth = $CI->db->where('id', $row->id)->from('ps_auths')->get()->row();
				$_aors = $CI->db->where('id', $row->id)->from('ps_aors')->get()->row();
				$_ep = $CI->db->where('id', $row->id)->from('ps_endpoints')->get()->row();
				$_ide = $CI->db->where('id', $row->id)->from('ps_endpoint_id_ips')->get()->row();
				$string .= "\n[".$row->id."]\n";
				$string .= "type=registration\n";
				$string .= "transport=".$row->transport."\n";
				$string .= "outbound_auth=".$row->id."\n";
				$string .= "server_uri=".$row->server_uri."\n";
				$string .= "client_uri=".$row->client_uri."\n";
				$string .= "contact_user=".$row->id."\n";
				$string .= "retry_interval=60\n";
				$string .= "forbidden_retry_interval=600\n";
				$string .= "expiration=3600\n";
				
				$string .= "\n[".$row->id."]\n";
				$string .= "type=auth\n";
				$string .= "auth_type=".$_auth->auth_type."\n";
				$string .= "password=".$_auth->password."\n";
				$string .= "username=".$_auth->username."\n\n";
 
				$string .= "\n[".$row->id."]\n";
				$string .= "type=aor\n";
				$string .= "contact=".$row->server_uri."\n\n";
 
				$string .= "\n[".$row->id."]\n";
				$string .= "type=endpoint\n";
				$string .= "transport=".$_ep->transport."\n";
				$string .= "context=".$_ep->context."\n";
				$string .= "disallow=all\n";
				$string .= "allow=".$_ep->allow."\n";
				$string .= "outbound_auth=".$row->id."\n";
				$string .= "aors=".$row->id."\n";
				$string .= "dtmf_mode=info\n\n";
				//$string .= "dtmf_mode=".$_ep->dtmf_mode."\n\n";
 
				$string .= "\n[".$row->id."]\n";
				$string .= "type=identify\n";
				$string .= "endpoint=".$row->id."\n";
				$string .= "match=".$_ide->match."\n\n";
			}
			$_data = $CI->db->get('ps_endpoints');
			$voicemail ="";
			foreach($_data->result() as $row) {
				if(preg_match("/SIP_/", $row->id) || preg_match("/TRUNK_/", $row->id)) {
					continue;
				}
				$_auth = $CI->db->where('id', $row->id)->from('ps_auths')->get()->row();
				$_aors = $CI->db->where('id', $row->id)->from('ps_aors')->get()->row();
				$string .= "\n[".$row->id."]\n";
				$string .= "type=endpoint\n";
				$string .= "context=".$row->context."\n";
				$string .= "disallow=all\n";
				$string .= "allow=".$row->allow."\n";
				$string .= "transport=".$row->transport."\n";
				$string .= "auth=".$row->id."\n";
				$string .= "aors=".$row->id."\n";
				$string .= "mailboxes=".$row->id."@".$row->context."\n";
				$string .= "aggregate_mwi=yes\n";
				$string .= "allow_subscribe=yes\n\n";
				//$string .= "dtmf_mode=".$row->dtmf_mode."\n\n";
				//$string .= "dtmf_mode=info\n\n";
				 
				$string .= "\n[".$row->id."]\n";
				$string .= "type=auth\n";
				$string .= "auth_type=".$_auth->auth_type."\n";
				$string .= "password=".$_auth->password."\n";
				$string .= "username=".$_auth->username."\n\n";
				 
				$string .= "\n[".$row->id."]\n";
				$string .= "type=aor\n";
				$string .= "max_contacts=".$_aors->max_contacts."\n\n";
				$vmcontext = $row->context;
				$voicemail .= $row->id." => ".$_auth->password.",".$row->id."\n";

			}
			$archiv = "/etc/asterisk/pjsip.proveedores.conf";
			file_put_contents($archiv, $string);
			$vmstring = $vmlegend;
			$vmstring .= "[".$vmcontext."]\n";
			$vmstring .= $voicemail;
			$_voicemail = "/etc/asterisk/voicemail.custom.conf";
			file_put_contents($_voicemail, $vmstring);
			//pbxSendCommand('pjsip reload');
			//pbxSendCommand('voicemail reload');
			pbxSendCommand('reload');
		}
	}
	/**
	 *Function pbxRegenerateRingGroup
	 */
	if ( ! function_exists('pbxRegenerateRingGroup')) {
		function pbxRegenerateRingGroup() {
			$string = ";--\n@Author\t\t Jose Luis Huamani Gonzales.\n@Dev\t\t Jose Luis Huamani Gonzales\n@Personal\t JoseLuisGonzales@mail.ru\n--;\n\n";
			$CI = & get_instance();
			$data = $CI->db->get('grupo_timbrado');
			$string .= "\n[ring_group]\n";
			foreach($data->result() as $row) {
				$string .= ";GRUPO => " . $row->gt_name . "\n";
				$string .= "exten => " . $row->gt_did .",1,Answer()\n";
				$string .= "same=>n,Set(typeOut=ENTRANTE)\n";
				$string .= 'same=>n,Set(CDR(type_call)=${typeOut})';
				$string .= "\nsame=>n,Set(preFolder=/var/spool/asterisk/monitor)";
				$string .= 'same=>n,Set(folderInbound=inbound/${STRFTIME(${EPOCH},,%Y)}/${STRFTIME(${EPOCH},,%m)}/${STRFTIME(${EPOCH},,%d)})';
				$string .= "\n";
				$string .= 'same=>n,System(mkdir -p ${preFolder}/${folderInbound})';
				$string .= "\n";
				$string .= 'same=>n,Set(audioFile=${folderInbound}/${UNIQUEID}-${typeOut}-${EXTEN}-${CALLERID(num)}-${STRFTIME(${EPOCH},,%Y-%m-%d-%H-%M-%S)}.wav)';
				$string .= "\n";
				$string .= 'same=>n,Set(CDR(audio)=${audioFile})';
				$string .= "\n";
				$string .= 'same=>n,MixMonitor(${audioFile})';
				$string .= "\n";
				$anexos = explode(',', $row->gt_anexos);
				$_p = '';
				if($row->gt_type == 0x0001) {
					if(count($anexos) >= 0x0001) {
						$t = 0;
						foreach($anexos as $k) {
							$_p .= "PJSIP/" . $k;
							$t++;
							if($t < count($anexos) )
								$_p .= "&";
						}
					} else {
						$_p .= "PJSIP/" . $anexos;
					}
					$string .= "same=>n,Dial(" . $_p .")\n";
				} else {
					if(count($anexos) >= 0x0001) {
						foreach($anexos as $k) {
							$string .= "same=>n,Dial(PJSIP/" . $k .")\n";
						}
					} else {
						$string .= "same=>n,Dial(PJSIP/" . $k .")\n";
					}
				}
				$string .= "same=>n,Hangup()\n\n\n";
			}
			$archiv = "/etc/asterisk/extensions.ring_group.conf";
			file_put_contents($archiv, $string);
			//pbxSendCommand('dialplan reload');
			pbxSendCommand('reload');
		}
	}
	/**
	 *Function generatePassword
	 */
	if ( ! function_exists('generatePassword')) {
		function generatePassword() {
    	$CI = & get_instance();
			$CI->db->where('cdp_key', 'max_char_password');
			$query = $CI->db->get('cdp_core_config');
			$length = $query->row()->cdp_value;
			$password = '';
    	$allowCharacters = '0123456789';
    	$maxlength = strlen($allowCharacters);
    	
			/* Esta parte si no queremos que se repitan los numeros en el password
			if ($length > $maxlength) {
				$length = $maxlength;
			}
			*/
			
			for($i=1;$i<=$length;$i++)
			{
				$char = substr($allowCharacters, mt_rand(0, $maxlength-1), 1);
				/* Esta parte si no queremos que se repitan los numeros en el password
				if (!strstr($_password, $char)) { 
					$_password .= $char;
				}
				*/
				$password .= $char;
			}
			$CI->db->where('pwd_text',$password);
			$CI->db->from('permisos');
			$pwd =$CI->db->count_all_results();
			//$existe = $this->homeModel->matchingPwd( md5($password) );
			if($pwd == '0')
			{
					return $password;
			} else {
				generatePassword($length);
			}
    	
    }
	}