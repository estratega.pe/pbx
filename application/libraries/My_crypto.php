<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Crypto //extends CI_Encrypt
{
	private $CI;
	public function __construct()
	{
		//parent::__construct();
		$this->CI =& get_instance();
	}
	
	public function password($data, $algo = "sha256")
	{
		if( ! $this->CI->config->item("encryption_key"))
		{
			show_error('Encryption key not found');
		}
		
		$hash = hash_init($algo, HASH_HMAC, $this->CI->config->item("encryption_key"));
		hash_update($hash, $data);
		return hash_final($hash);
	}
}
// END MY_Crypto class

/* End of file MY_Crypto.php */
/* Location: ./applicatiion/libraries/MY_Crypto.php */