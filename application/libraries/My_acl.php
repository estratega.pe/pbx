<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Acl {
	private $CI;
	private $_id;
	private $_role;
	private $_permisos;
	
	static $_tables = array(
		'users'			=> 'user'
		,'roles'		=> 'role'
		,'perms'		=> 'permission'
		,'user_perms'	=> 'user_permission'
		,'role_perms'	=> 'role_permission'
	);
	
	public function __construct($id = false)
	{
		$this->CI =& get_instance();
		if($id)
		{
			$this->_id = (int) $id;
		} else {
			$this->_id = $this->CI->session->userdata('userID');
		}
		if($this->_id > 0) {
			$this->_role = $this->getRole();
			$this->_permisos = $this->getPermisosRole();
			$this->compilarAcl();
		}
	}
	
	public function compilarAcl()
	{
		$this->_permisos = array_merge(
			$this->_permisos,
			$this->getPermisosUsuario()
		);
	}
	
	public function getRole()
	{
		$role = $this->CI->db
			->select('role')
			->get_where(self::$_tables['users'], array('user_id' => $this->_id))
			->row('role');
		return $role;
	}
	
	public function getPermisosRoleId()
	{
		$id = array();
		$perms = $this->CI->db
			->select('perm')
			->get_where(self::$_tables['role_perms'], array('role' => $this->_role))
			->result_array();
		for($i = 0; $i < count($perms); $i++){
			$id[] = $perms[$i]['perm'];
		}
		return $id;
	}
	
	public function getPermisosRole()
	{
		$permisos = $this->CI->db
			->select('*')
			->get_where(self::$_tables['role_perms'], array('role' => $this->_role))
			->result();
		$data = array();
		for($i = 0; $i < count($permisos); $i++)
		{
			$key = $this->getPermisoKey($permisos[$i]->perm);
			if($key == ''){continue;}
			if($permisos[$i]->value == 1)
			{
				$v = true;
			} else {
				$v = false;
			}
			$data[$key] = array(
				'key'		=> $key
				,'permiso'	=> $this->getPermisoNombre($permisos[$i]->perm)
				,'value'	=> $v
				,'heredado'	=> true
				,'id'		=> $permisos[$i]->perm
			);
		}
		return $data;
	}
	
	public function getPermisoKey($permisoID)
	{
		$permisoID = (int) $permisoID;
		$key = $this->CI->db
				->select('key')
				->get_where(self::$_tables['perms'], array('permission_id' => $permisoID))
				->row('key');
		return $key;
	}
	
	public function getPermisoNombre($permisoID)
	{
		$permisoID = (int) $permisoID;
		$permiso = $this->CI->db
				->select('perm')
				->get_where(self::$_tables['perms'], array('permission_id' => $permisoID))
				->row();
		return $permiso;
	}
	
	public function getPermisosUsuario()
	{
		$data = array();
		$ids = $this->getPermisosRoleId();
		if(sizeof($ids) > 0)
		{
			$permissions = $this->CI->db->query(
				"SELECT * FROM ".self::$_tables['user_perms']
				." WHERE user_id = '".$this->_id."' " .
				"AND perm IN (" . implode(",", $ids) . ")"
				)->result();
			for($i = 0; $i < count($permissions); $i++)
			{
				$key = $this->getPermisoKey($permissions[$i]->perm);
				if($key == ''){continue;}
				if($permissions[$i]->value == 1)
				{
					$v = TRUE;
				} else {
					$v = FALSE;
				}
				$data[$key] = array(
					'key'			=> $key
					,'perms'		=> $this->getPermisoNombre($permissions[$i]->perm)
					,'value'		=> $v
					,'inherited'	=> false
					,'id' 			=> $permissions[$i]->perm
				);
			}
		}
		return $data;
	}
	
	public function getPermisos()
	{
		if(isset($this->_permisos) && count($this->_permisos))
			return $this->_permisos;
	}
	
	public function permiso($key)
	{
		if(array_key_exists($key, $this->_permisos))
		{
			if($this->_permisos[$key]['value'] == true || $this->_permisos[$key]['value'] == 1)
			{
				return true;
			}
		}
		return false;
	}
	
	public function acceso($key)
	{
		if($this->permiso($key))
		{
			return TRUE;
		}
		return FALSE;
	}
}

// END MY_Acl class

/* End of file MY_Acl.php */
/* Location: ./application/libraries/Acl.php */