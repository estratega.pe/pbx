<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Login
{
	private $CI;
	private $pattern = "/^([-a-z0-9_-])+$/i";
	private $errors = array();
	static $_table = 'user';
	
	public function __construct()
	{
		$this->CI =& get_instance();
	}
	
	public function loginIn($data, $hash = 'sha256')
	{
		$user = $data['user'];
		$pass = $data['password'];
		$row = array();
		if(empty($user) || ! preg_match($this->pattern, $user))
		{
			//$this->errors['invalid_user'] = "Invalid user";
			$this->errors = "Invalid user";
		}
		if(empty($pass))
		{
			//$this->errors['empty_password'] = "Password is empty";
			$this->errors = "Password is empty";
		}
		if(sizeof($this->errors) == 0)
		{
			$row = $this->_row(array('username' => $user, 'password' => $this->CI->my_crypto->password($pass, $hash)));
			if(sizeof($row) == 0 || $row->active != 1 || $row->status != 1)
			{
				//$this->errors['bad_credentials'] = "User/Password incorrect";
				$this->errors = "User/Password incorrect";
			}
		}
		if(sizeof($row) == 1)
		{
			$this->_load($row);
			return true;
		} else {
			$this->CI->session->set_flashdata('errors', $this->errors);
			return false;
		}
	}
	
	public function loginOut()
	{
		$this->CI->session->sess_destroy();
		return true;
	}
	
	private function _load($row)
	{
		$data = array(
			'username'	=> $row->username
			,'first_name'		=> $row->first_name
			,'last_name'	=> $row->last_name
			,'role'		=> $row->role
			,'group'	=> $row->grupo
			,'email'	=> $row->email
			,'active'	=> $row->active
			,'status'	=> $row->status
		);
		$this->CI->session->set_userdata('userID', $row->user_id);
		$this->CI->session->set_userdata('userDATA', $data);
	}
	
	private function _row($where = null, $select = null)
	{
		if(is_array($where))
		{
			$this->CI->db->where($where);
		}
		if(is_array($select))
		{
			$this->CI->db->select($select);
		}
		return $this->CI->db->get(self::$_table)->row();
	}
}
// END MY_Login class

/* End of file MY_Login.php */
/* Location: ./application/libraries/Login.php */