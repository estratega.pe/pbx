<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>			
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				
				<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
						{PAGE_TITLE}
            <small>{PAGE_DESCRIPTION}</small>
          </h1>
          <ol class="breadcrumb">
						{BREADCRUMBS}
          </ol>
        </section>
				
				<!-- Main content -->
        <section class="content">
				{CONTENT_PAGE}
				</section>
				
			</div>