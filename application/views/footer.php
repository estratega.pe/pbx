<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>			
			<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.1
        </div>
        <strong>Copyright &copy; 2015 <a href="http://consultoresdeprimera.com" target="_blank">Consultores de Primera</a>.</strong> All rights reserved.
      </footer>
		<div class="control-sidebar-bg"></div>
	</div><!-- ./wrapper -->

		{CSS}
			<link rel="stylesheet" href="{FILE}">
		{/CSS}
		<!-- jQuery 2.1.4 -->
    <script src="{BASE_URL}assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{BASE_URL}assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Slimscroll -->
    <script src="{BASE_URL}assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="{BASE_URL}assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{BASE_URL}assets/dist/js/app.min.js"></script>
		{JS}
		<script src="{FILE}"></script>
		{/JS}
		{JS_INLINE}
		<script>
			$(function () {
					{TEXT}
			});
		</script>
		{/JS_INLINE}
  </body>
</html>