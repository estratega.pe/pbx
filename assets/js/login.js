$(document).ready(function() {
	tb = $('input,button');

	/*if ($.browser.mozilla) {
		$(tb).keypress(enter2tab);
	} else {
		$(tb).keydown(enter2tab);
	}*/
	$(tb).keydown(enter2tab);
});

function enter2tab(e) {
	if (e.keyCode == 13) {
		e.preventDefault();
		cb = parseInt($(this).attr('tabindex'));
		if(cb == 2) {
			$('.form-horizontal').submit();
			
			return false;
		}
		if ($(':input[tabindex=\'' + (cb + 1) + '\']') != null) {
			$(':input[tabindex=\'' + (cb + 1) + '\']').focus();
			$(':input[tabindex=\'' + (cb + 1) + '\']').select();
			return false;
		}
	}
}

